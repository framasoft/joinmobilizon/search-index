import { Router, Request, Response } from 'express'
import { asyncMiddleware } from '../../middlewares/async'
import { methodsValidator } from '../../middlewares/validators/method'
import { formatGroupMarkerForAPI, queryGroupMarkers } from '../../lib/elastic-search/elastic-search-group-markers'
import { GroupMarkersSearchQuery } from '../../types/search-query/group-search.model'
import { Searcher } from '../../lib/controllers/searcher'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { locatedContentSearchValidator } from '../../middlewares/validators/search'
const searchGroupMarkersRouter = Router()

searchGroupMarkersRouter.all(
  '/search/group-markers',
  methodsValidator(['POST', 'GET']),
  locatedContentSearchValidator,
  setDefaultSearchSort,
  asyncMiddleware(searchGroupMarkers)
)

// ---------------------------------------------------------------------------

export { searchGroupMarkersRouter }

async function searchGroupMarkers(req: Request, res: Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as GroupMarkersSearchQuery

  const searcher = new Searcher(queryGroupMarkers, formatGroupMarkerForAPI)
  const { data } = await searcher.getResult(query)
  return res.json({
    type: 'FeatureCollection',
    features: data,
  })
}
