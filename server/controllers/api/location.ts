import { Router, Request, Response } from 'express'
import { asyncMiddleware } from '../../middlewares/async'
import { methodsValidator } from '../../middlewares/validators/method'
import { geocode } from '../../lib/geocoding'
import { search as searchPicture } from '../../lib/pictures'
import { geoLocationSearchValidator } from '../../middlewares/validators/search'
import { PictureInformation } from '../../../shared/picture'
import { logger } from '../../helpers/logger'

const geoLocationRouter = Router()

geoLocationRouter.all(
  '/location',
  methodsValidator(['POST', 'GET']),
  geoLocationSearchValidator,
  asyncMiddleware(locationInformation)
)

// ---------------------------------------------------------------------------

export { geoLocationRouter }

// ---------------------------------------------------------------------------

async function locationInformation(req: Request, res: Response) {
  const result = await geocode(parseFloat(req.query.latitude as string), parseFloat(req.query.longitude as string))
  logger.debug(result, 'result from geocoding')
  if (!result) {
    return res.status(404).send()
  }
  let picture_info: PictureInformation | undefined
  if (result.address?.addressLocality) {
    picture_info = await searchPicture(result.address?.addressLocality)
  }

  return res.json({ ...result, picture_info })
}
