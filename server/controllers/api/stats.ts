import { Router, Request, Response } from 'express'
import { categoryStats } from '../../lib/elastic-search/elastic-search-events'
import { asyncMiddleware } from '../../middlewares/async'
import { methodsValidator } from '../../middlewares/validators/method'
import { categoryStatsValidator } from '../../middlewares/validators/search'

const statsRouter = Router()

statsRouter.all(
  '/stats/categories',
  methodsValidator(['POST', 'GET']),
  categoryStatsValidator,
  asyncMiddleware(categoriesStatistics)
)

// ---------------------------------------------------------------------------

export { statsRouter }

// ---------------------------------------------------------------------------

async function categoriesStatistics(req: Request, res: Response) {
  const result = await categoryStats((req.query.languageOneOf as string[]) ?? [])

  return res.json(result)
}
