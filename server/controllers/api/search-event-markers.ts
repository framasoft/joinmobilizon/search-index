import { Router, Request, Response } from 'express'
import { asyncMiddleware } from '../../middlewares/async'
import { methodsValidator } from '../../middlewares/validators/method'
import { formatEventMakerForAPI, queryEventMarkers } from '../../lib/elastic-search/elastic-search-event-markers'
import { EventMarkersSearchQuery } from '../../types/search-query/event-search.model'
import { Searcher } from '../../lib/controllers/searcher'
import { locatedContentSearchValidator } from '../../middlewares/validators/search'
const searchEventMarkersRouter = Router()

searchEventMarkersRouter.all(
  '/search/event-markers',
  methodsValidator(['POST', 'GET']),
  locatedContentSearchValidator,
  asyncMiddleware(searchEventMarkers)
)

// ---------------------------------------------------------------------------

export { searchEventMarkersRouter }

async function searchEventMarkers(req: Request, res: Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as EventMarkersSearchQuery

  const searcher = new Searcher(queryEventMarkers, formatEventMakerForAPI)
  const { data } = await searcher.getResult(query)
  return res.json({
    type: 'FeatureCollection',
    features: data,
  })
}
