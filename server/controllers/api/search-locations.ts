import { Router, Request, Response } from 'express'
import { asyncMiddleware } from '../../middlewares/async'
import { methodsValidator } from '../../middlewares/validators/method'
import { locationsSearchValidator } from '../../middlewares/validators/search'
import { search } from '../../lib/geocoding'

const searchLocationsRouter = Router()

searchLocationsRouter.all(
  '/search/locations',
  methodsValidator(['POST', 'GET']),
  locationsSearchValidator,
  asyncMiddleware(searchLocations)
)

export { searchLocationsRouter }

async function searchLocations(req: Request, res: Response) {
  return res.json(await search(req.query.search as string))
}
