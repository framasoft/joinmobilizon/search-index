import { Router, Request, Response } from 'express'
import { badRequest } from '../../helpers/utils'
import { configRouter } from './config'
import { searchGroupsRouter } from './search-groups'
import { searchPostsRouter } from './search-posts'
import { searchEventsRouter } from './search-events'
import { searchEventMarkersRouter } from './search-event-markers'
import { searchGroupMarkersRouter } from './search-group-markers'
import { searchLocationsRouter } from './search-locations'
import { geoLocationRouter } from './location'
import { statsRouter } from './stats'

const apiRouter = Router()

apiRouter.use('/', configRouter)
apiRouter.use('/', searchEventsRouter)
apiRouter.use('/', searchEventMarkersRouter)
apiRouter.use('/', searchGroupsRouter)
apiRouter.use('/', searchGroupMarkersRouter)
apiRouter.use('/', searchPostsRouter)
apiRouter.use('/', searchLocationsRouter)
apiRouter.use('/', geoLocationRouter)
apiRouter.use('/', statsRouter)
apiRouter.use('/ping', pong)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }

// ---------------------------------------------------------------------------

function pong(req: Request, res: Response) {
  return res.send('pong').status(200).end()
}
