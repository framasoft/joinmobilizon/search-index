import { Router, Request, Response } from 'express'
import { ServerConfig } from '../../../shared'
import { CONFIG } from '../../initializers/constants'
import { IndexationScheduler } from '../../lib/schedulers/indexation-scheduler'
import { GeoIP } from '../../helpers/geo-ip'

const configRouter = Router()

configRouter.get('/config', getConfig)

// ---------------------------------------------------------------------------

export { configRouter }

// ---------------------------------------------------------------------------

async function getConfig(req: Request, res: Response) {
  return res.json({
    searchInstanceName: CONFIG.SEARCH_INSTANCE.NAME,
    searchInstanceNameImage: CONFIG.SEARCH_INSTANCE.NAME_IMAGE,
    searchInstanceSearchImage: CONFIG.SEARCH_INSTANCE.SEARCH_IMAGE,
    legalNoticesUrl: CONFIG.SEARCH_INSTANCE.LEGAL_NOTICES_URL,
    indexedHostsCount: IndexationScheduler.Instance.getIndexedHosts().length,
    indexedInstancesUrl: CONFIG.INSTANCES_INDEX.PUBLIC_URL,
    ipLocation: await GeoIP.Instance.safeCountryISOLookup(req),
  } as ServerConfig)
}
