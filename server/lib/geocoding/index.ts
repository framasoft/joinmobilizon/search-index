import {
  search as peliasSearch,
  autocomplete as peliasAutocomplete,
  geocode as peliasGeocode,
  countryInfo as peliasCountryInfo,
} from './providers/pelias'
import {
  search as nominatimSearch,
  geocode as nominatimGeoCode,
  countryInformation as nominatimCountryInfo,
} from './providers/nominatim'
import { CONFIG } from '../../initializers/constants'
import { ResultList } from '../../types/search-query/common-search.model'
import { AutoCompleteLocation, Location } from '../../types/address.model'
import { FeatureCollection, Position } from 'geojson'

export async function autocomplete(location: string): Promise<ResultList<AutoCompleteLocation>> {
  switch (CONFIG.GEOCODING.TYPE) {
    case 'pelias':
      return await peliasAutocomplete(location, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    default:
      throw new Error('No location provider registered')
  }
}

export async function search(location: string): Promise<ResultList<Location>> {
  switch (CONFIG.GEOCODING.TYPE) {
    case 'pelias':
      return await peliasSearch(location, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    case 'nominatim':
      return await nominatimSearch(location, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    default:
      throw new Error('No location provider registered')
  }
}

export async function countryInfo(country: string): Promise<Position> {
  switch (CONFIG.GEOCODING.TYPE) {
    case 'pelias':
      return await peliasCountryInfo(country, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    case 'nominatim':
      return await nominatimCountryInfo(country, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    default:
      throw new Error('No location provider registered')
  }
}

export async function geocode(latitude: number, longitude: number): Promise<Location> {
  switch (CONFIG.GEOCODING.TYPE) {
    case 'pelias':
      return await peliasGeocode(latitude, longitude, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    case 'nominatim':
      return await nominatimGeoCode(latitude, longitude, {
        endpoint: CONFIG.GEOCODING.ENDPOINT,
      })
    default:
      throw new Error('No location provider registered')
  }
}
