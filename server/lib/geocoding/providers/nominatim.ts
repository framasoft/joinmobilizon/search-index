import { ProviderOptions } from '../../../types/geocoding'
import { logger } from '../../../helpers/logger'
import { doJSONRequest } from '../../../helpers/requests'
import { BasicNominatimSearchResult, NominatimSearchResult } from '../../../types/geocoding/nominatim'
import { Position } from 'geojson'
import { nominatimResultsToLocationResults, nominatimResultToLocationResult } from '../helper'
import { ResultList } from '../../../types/search-query/common-search.model'
import { Location } from '../../../types/address.model'

export const NOMINATIM_SEARCH_API = '/search'
export const NOMINATIM_REVERSE_GEOCODING_API = '/reverse'

export async function search(location: string, options: ProviderOptions): Promise<ResultList<Location>> {
  const url = `${options.endpoint}${NOMINATIM_SEARCH_API}?city=${location}&format=jsonv2`
  try {
    const res = await doJSONRequest<BasicNominatimSearchResult[]>(url)
    return nominatimResultsToLocationResults(res.body)
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Nominatim')
  }
}

export async function countryInformation(country: string, options: ProviderOptions): Promise<Position> {
  const url = `${options.endpoint}${NOMINATIM_SEARCH_API}?country=${country}&format=jsonv2`
  try {
    const res = await doJSONRequest<NominatimSearchResult>(url)
    return res.body.centroid.coordinates
  } catch (err) {
    logger.error({ err }, 'Failure fetching country details from Nominatim')
  }
}

export async function geocode(latitude: number, longitude: number, options: ProviderOptions): Promise<Location> {
  const url = `${options.endpoint}${NOMINATIM_REVERSE_GEOCODING_API}?lat=${latitude}&lon=${longitude}&format=jsonv2`
  try {
    logger.debug(`Asking nominatim to geocode ${url}`)
    const res = await doJSONRequest<BasicNominatimSearchResult>(url)
    return nominatimResultToLocationResult(res.body)
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Nominatim')
  }
}
