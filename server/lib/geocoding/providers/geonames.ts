import { ProviderOptions } from '../../../types/geocoding'
import { logger } from '../../../helpers/logger'
import { doJSONRequest } from '../../../helpers/requests'
import { GeoNamesCountryInformation } from '../../../types/geocoding/geonames'
import { Position } from 'geojson'
import { computeCentroid } from '../helper'

export const GEONAMES_COUNTRY_INFORMATION_API = '/countryInfoJSON?username=tcit&country=FR'

export async function countryInformation(country: string, options: ProviderOptions): Promise<Position> {
  const url = `${options.endpoint}${GEONAMES_COUNTRY_INFORMATION_API}?username=${options.accessKey}country=${country}`
  try {
    const res = await doJSONRequest<GeoNamesCountryInformation>(url)
    return computeCentroid(res.body.geonames[0])
  } catch (err) {
    logger.error({ err }, 'Failure fetching country details from Geonames')
  }
}
