import { ProviderOptions } from '../../../types/geocoding'
import { FeatureCollection, Point, Position } from 'geojson'
import { ResultList } from '../../../types/search-query/common-search.model'
import { geoJSONToLocationResult } from '../helper'
import { logger } from '../../../helpers/logger'
import { doJSONRequest } from '../../../helpers/requests'
import { Location } from '../../../types/address.model'

export const PELIAS_SEARCH_API = '/v1/search'
export const PELIAS_AUTOCOMPLETE_API = '/v1/autocomplete'
export const PELIAS_REVERSE_GEOCODING_API = '/v1/reverse'

export async function search(location: string, options: ProviderOptions): Promise<ResultList<Location>> {
  try {
    const res = await doSearch(location, options)
    return geoJSONToLocationResult(res)
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Pelias')
  }
}

export async function autocomplete(location: string, options: ProviderOptions): Promise<ResultList<Location>> {
  const url = `${options.endpoint}${PELIAS_AUTOCOMPLETE_API}?text=${location}&layers=coarse`
  try {
    const res = await doJSONRequest<FeatureCollection<Point>>(url)
    return geoJSONToLocationResult(res.body)
  } catch (err) {
    logger.error({ err }, 'Failure fetching autocomplete location details from Pelias')
  }
}

export async function geocode(latitude: number, longitude: number, options: ProviderOptions): Promise<Location> {
  const url = `${options.endpoint}${PELIAS_REVERSE_GEOCODING_API}?point.lat=${latitude}&point.lon=${longitude}`
  try {
    const res = await doJSONRequest<FeatureCollection<Point>>(url)
    return geoJSONToLocationResult(res.body).data[0]
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Pelias')
  }
}

export async function countryInfo(country: string, options: ProviderOptions): Promise<Position> {
  const res = await doSearch(country, options)
  return res.features[0].geometry.coordinates
}

async function doSearch(location: string, options: ProviderOptions): Promise<FeatureCollection<Point>> {
  const url = `${options.endpoint}${PELIAS_SEARCH_API}?text=${location}&layers=coarse`
  try {
    logger.debug({ url }, 'Calling pelias for search')
    const res = await doJSONRequest<FeatureCollection<Point>>(url)
    return res.body
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Pelias')
  }
}
