import { FeatureCollection, Point, Position } from 'geojson'
import { ResultList } from '../../types/search-query/common-search.model'
import { Location } from '../../types/address.model'
import { BasicNominatimSearchResult } from '../../types/geocoding/nominatim'

export function geoJSONToLocationResult(geoJSON: FeatureCollection<Point>): ResultList<Location> {
  const rawData = geoJSON.features.map(
    ({
      geometry: {
        coordinates: [lon, lat],
      },
      properties: { name, id, label, postalcode, region, locality, country },
    }) => ({
      location: { lon, lat },
      name: label,
      id,
      address: {
        addressCountry: country,
        addressLocality: locality,
        addressRegion: region,
        postalCode: postalcode,
        streetAddress: name,
      },
    })
  )
  return { total: rawData.length, data: rawData }
}

export function geoJSONToAddressResult(geoJSON: FeatureCollection<Point>): ResultList<Location> {
  const rawData = geoJSON.features.map(
    ({
      geometry: {
        coordinates: [lon, lat],
      },
      properties: { gid, name, country, region, locality, postalcode, street },
    }) => ({
      id: gid,
      address: {
        addressCountry: country,
        addressLocality: locality,
        addressRegion: region,
        postalCode: postalcode,
        streetAddress: street,
      },
      location: { lon, lat },
      name,
    })
  )
  return { total: rawData.length, data: rawData }
}

export function computeCentroid({ north, south, east, west }): Position {
  return [(north + south) / 2, (east + west) / 2]
}

export function nominatimResultsToLocationResults(
  nominatimResults: BasicNominatimSearchResult[]
): ResultList<Location> {
  return {
    total: nominatimResults.length,
    data: nominatimResults.map(nominatimResultToLocationResult),
  }
}

export function nominatimResultToLocationResult({ osm_id, display_name, lon, lat, address }) {
  return {
    id: osm_id,
    name: display_name,
    location: {
      lon: parseFloat(lon),
      lat: parseFloat(lat),
    },
    address: {
      addressCountry: address?.country,
      addressLocality: address?.city || address?.village || address?.municipality,
      streetAddress: address?.house_number ? `${address?.house_number} ${address?.road}`.trim() : address?.road,
      addressRegion: address?.state || address?.region,
      postalCode: address?.postcode,
    },
  }
}
