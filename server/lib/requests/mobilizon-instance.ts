import { APPost, IndexablePost } from '../../types/post.model'
import { doJSONRequest } from '../../helpers/requests'
import { INDEXER_COUNT, REQUESTS } from '../../initializers/constants'
import { IndexableGroup } from '../../types/group.model'
import { IndexableDoc } from '../../types/indexable-doc.model'
import { APEvent, IndexableEvent } from '../../types/event.model'
import { APObject, OrderedCollectionPage } from '../../types/activity-pub'
import { APActor, APGroup } from '../../types/actor.model'
import { logger } from '../../helpers/logger'

async function getActor(url: string, actorsCache: Map<string, any>): Promise<APActor> {
  if (actorsCache.has(url)) {
    logger.debug(`Using actor cache for ${url}`)
    return actorsCache.get(url)
  }

  if (url.split('@').length - 1 > 1) return null

  await new Promise((resolve) => setTimeout(resolve, 1000))
  logger.debug(`Getting actor at ${url}`)
  const res = await doJSONRequest<APActor>(url, {
    activityPub: true,
  })

  actorsCache.set(url, res.body)

  return res.body
}

async function getEvent(host: string, uuid: string): Promise<IndexableEvent> {
  const url = 'https://' + host + '/events/' + uuid
  logger.debug(`Getting event at ${url}`)

  const res = await doJSONRequest<APEvent>(url, {
    activityPub: true,
  })

  return prepareEventForDB(res.body, host)
}

async function getGroup(host: string, name: string): Promise<IndexableGroup> {
  const res = await doJSONRequest<APGroup>(name, {
    activityPub: true,
  })

  return prepareGroupForDB(res.body, host)
}

async function fetchOutbox(host: string, page: number): Promise<{ events: IndexableEvent[]; posts: IndexablePost[] }> {
  const url = 'https://' + host + '/@relay/outbox'

  const res = await doJSONRequest<OrderedCollectionPage<APEvent | APPost>>(url, {
    activityPub: true,
    searchParams: {
      page,
      filter: 'local',
      skipCount: true,
      count: INDEXER_COUNT,
    },
  })

  let items = res.body.orderedItems.map((i) => i.object).filter((i) => filterLocal(i, host))
  return items.reduce(
    (acc, item) => {
      if (item.type === 'Event') {
        acc.events.push(item)
      }
      if (item.type === 'Post') {
        acc.posts.push(item)
      }
      return acc
    },
    {
      events: [],
      posts: [],
    }
  )
}

function prepareEventForDB<T extends APEvent>(event: T, host: string): APEvent & IndexableDoc {
  return Object.assign(event, {
    elasticSearchId: `${host}:${event.uuid}`,
    host,
    url: 'https://' + host + '/events/' + event.uuid,
  })
}

function preparePostForDB<T extends APPost>(post: T, host: string): APPost & IndexableDoc {
  return Object.assign(post, {
    elasticSearchId: `${host}:${post.uuid}`,
    host,
    url: 'https://' + host + '/p/' + post.uuid,
  })
}

function prepareGroupForDB(group: APGroup, host: string): IndexableGroup {
  return Object.assign(group, {
    elasticSearchId: `${host}:${group.preferredUsername}`,
    host,
    handle: `${group.preferredUsername}@${host}`,
    url: 'https://' + host + '/@' + group.preferredUsername,
  })
}

async function fetchAllRelations<T extends APEvent | APPost>(entity: T, actorsCache: Map<string, any>): Promise<T> {
  if (typeof entity.actor === 'string') {
    entity.actor = await getActor(entity.actor, actorsCache)
  }
  if (typeof entity.attributedTo === 'string') {
    entity.attributedTo = await getActor(entity.attributedTo, actorsCache)
  }
  return entity
}

function filterLocal({ id }: APObject, host: string): boolean {
  const url = new URL(id)
  return url.host === host
}

export { getEvent, getGroup, fetchOutbox, prepareEventForDB, prepareGroupForDB, preparePostForDB, fetchAllRelations }
