import { CONFIG } from '../../initializers/constants'
import { doJSONRequest } from '../../helpers/requests'
import { RemoteInstance } from '../../../shared/remote-instance'
import { ResultList } from '../../types/search-query/common-search.model'

async function listIndexInstancesHost(): Promise<string[]> {
  const uri = CONFIG.INSTANCES_INDEX.URL

  const searchParams = {
    healthy: true,
    count: 5000,
  }

  const { body } = await doJSONRequest<ResultList<RemoteInstance>>(uri, { searchParams })

  return body.data.map((o) => o.host)
}

export { listIndexInstancesHost }
