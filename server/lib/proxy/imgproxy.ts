import Imgproxy from 'imgproxy'
import { GravityType } from 'imgproxy/dist/types'
import { CONFIG } from '../../initializers/constants'

const imgproxy = new Imgproxy({
  baseUrl: CONFIG.IMG_PROXY.BASE_URL,
  key: CONFIG.IMG_PROXY.KEY,
  salt: CONFIG.IMG_PROXY.SALT,
  encode: true,
})

export function proxifyAuthorThumbnail(url: string | undefined): string | undefined {
  if (!url || !CONFIG.IMG_PROXY.ENABLED) return url
  return imgproxy.builder().resize('fill', 80, 80, false).gravity(GravityType.north_east).generateUrl(url)
}

export function proxifyGroupThumbnail(url: string | undefined): string | undefined {
  if (!url || !CONFIG.IMG_PROXY.ENABLED) return url
  return imgproxy.builder().resize('fill', 320, 320, false).gravity(GravityType.north_east).generateUrl(url)
}

export function proxifyEventThumbnail(url: string | undefined): string | undefined {
  if (!url || !CONFIG.IMG_PROXY.ENABLED) return url
  return imgproxy.builder().resize('fill', 572, 320, false).gravity(GravityType.smart).generateUrl(url)
}
