import { logger } from '../../helpers/logger'
import { CONFIG } from '../../initializers/constants'
import { DBEvent, IndexableEvent } from '../../types/event.model'
import { buildEventsMapping, formatEventForDB } from '../elastic-search/elastic-search-events'
import { getEvent } from '../requests/mobilizon-instance'
import { AbstractIndexer } from './shared'

export class EventIndexer extends AbstractIndexer<IndexableEvent, DBEvent> {
  constructor() {
    super(CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS, formatEventForDB)
  }

  async indexSpecificElement(host: string, uuid: string) {
    logger.debug('Indexing specific event %s on %s', uuid, host)
    const event = await getEvent(host, uuid)

    logger.debug('Indexing specific event %s of %s.', uuid, host)

    return this.indexElements([event], true)
  }

  buildMapping() {
    return buildEventsMapping()
  }
}
