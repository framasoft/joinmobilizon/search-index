import Bluebird from 'bluebird'
import { IndexablePost } from 'server/types/post.model'
import { inspect } from 'util'
import { logger } from '../../helpers/logger'
import {
  INDEXER_HOST_CONCURRENCY,
  INDEXER_COUNT,
  INDEXER_LIMIT,
  SCHEDULER_INTERVALS_MS,
} from '../../initializers/constants'
import { IndexableEvent } from '../../types/event.model'
import { buildInstanceHosts } from '../elastic-search/elastic-search-instances'
import { GroupIndexer } from '../indexers/group-indexer'
import { PostIndexer } from '../indexers/post-indexer'
import { EventIndexer } from '../indexers/event-indexer'
import { fetchAllRelations, fetchOutbox, prepareEventForDB, preparePostForDB } from '../requests/mobilizon-instance'
import { AbstractScheduler } from './abstract-scheduler'

export class IndexationScheduler extends AbstractScheduler {
  // eslint-disable-next-line no-use-before-define
  private static instance: IndexationScheduler

  protected schedulerIntervalMs = SCHEDULER_INTERVALS_MS.indexation

  private indexedHosts: string[] = []

  private readonly groupIndexer: GroupIndexer
  private readonly eventIndexer: EventIndexer
  private readonly postIndexer: PostIndexer

  private readonly indexers: [GroupIndexer, EventIndexer, PostIndexer]

  private constructor() {
    super()

    this.groupIndexer = new GroupIndexer()
    this.eventIndexer = new EventIndexer()
    this.postIndexer = new PostIndexer()

    this.indexers = [this.groupIndexer, this.eventIndexer, this.postIndexer]
  }

  async initIndexes() {
    return Promise.all(this.indexers.map((i) => i.initIndex()))
  }

  getIndexedHosts() {
    return this.indexedHosts
  }

  protected async internalExecute() {
    return this.runIndexer()
  }

  private async runIndexer() {
    logger.info('Running indexer.')

    const { indexHosts, removedHosts } = await buildInstanceHosts()
    this.indexedHosts = indexHosts

    for (const o of this.indexers) {
      await o.removeFromHosts(removedHosts)
    }

    await Bluebird.map(
      indexHosts,
      async (host) => {
        try {
          await this.indexHost(host)
        } catch (err) {
          console.error(inspect(err, { depth: 10 }))
          logger.warn({ err: inspect(err) }, 'Cannot index events from %s.', host)
        }
      },
      { concurrency: INDEXER_HOST_CONCURRENCY }
    )

    for (const o of this.indexers) {
      await o.refreshIndex()
    }

    logger.info('Indexer ended.')
  }

  private async indexHost(host: string) {
    const groupsToSync = new Set<string>()
    const existingGroupsId = new Set<string>()
    const existingEventsId = new Set<string>()
    const actorsCache = new Map<string, any>()

    let events: IndexableEvent[] = []
    let posts: IndexablePost[] = []
    let page = 1

    logger.info('Adding event data from %s.', host)

    do {
      logger.info('Getting event results from %s (from = %d).', host, page)

      const results = await fetchOutbox(host, page)

      events = await Promise.all(
        results.events.map(async (e) => {
          let event = await fetchAllRelations(e, actorsCache)
          return prepareEventForDB(event, host)
        })
      )
      posts = await Promise.all(
        results.posts.map(async (e) => {
          let post = await fetchAllRelations(e, actorsCache)
          return preparePostForDB(post, host)
        })
      )

      logger.info('Got %d event results from %s (from = %d).', events.length, host, page)

      page += 1

      if (events.length !== 0) {
        await this.eventIndexer.indexElements(events)

        logger.debug('Indexed %d events from %s.', events.length, host)

        // await this.postIndexer.indexElements(posts)

        logger.debug('Indexed %d posts from %s.', posts.length, host)
      }

      for (const event of events) {
        const creator = typeof event.actor !== 'string' ? event.actor?.id : event.actor
        const group = typeof event.attributedTo !== 'string' ? event.attributedTo?.id : event.attributedTo
        if (group !== creator) {
          logger.debug(`Adding ${group} to sync`)
          groupsToSync.add(group)

          existingGroupsId.add(group)
          existingEventsId.add(event.id)
        }
      }
    } while (events.length === INDEXER_COUNT && page < INDEXER_LIMIT)

    logger.info('Added event data from %s.', host)

    for (const c of groupsToSync) {
      this.groupIndexer.scheduleIndexation(host, c)
    }

    // await this.groupIndexer.removeNotExisting(host, existingGroupsId)
    // await this.eventIndexer.removeNotExisting(host, existingEventsId)

    // await this.indexPlaylists(host, Array.from(groupsToSync))
  }

  // private async indexPosts (host: string, channelHandles: string[]) {
  //   const existingPlaylistsId = new Set<number>()

  //   logger.info('Adding playlist data from %s.', host)

  //   for (const channelHandle of channelHandles) {
  //     let playlists: IndexablePost[] = []
  //     let start = 0

  //     do {
  //       logger.debug('Getting posts results from %s (from = %d, channelHandle = %s).', host, start, channelHandle)

  //       playlists = await getPlaylistsOf(host, channelHandle, start)
  //       logger.debug('Got %d playlist results from %s (from = %d, channelHandle = %s).', playlists.length, host, start, channelHandle)

  //       start += playlists.length

  //       if (playlists.length !== 0) {
  //         await this.playlistIndexer.indexElements(playlists)

  //         logger.debug('Indexed %d playlists from %s.', playlists.length, host)
  //       }

  //       for (const playlist of playlists) {
  //         existingPlaylistsId.add(playlist.id)
  //       }
  //     } while (playlists.length === INDEXER_COUNT && start < INDEXER_LIMIT)
  //   }

  //   logger.info('Added playlist data from %s.', host)

  //   await this.playlistIndexer.removeNotExisting(host, existingPlaylistsId)
  // }

  static get Instance() {
    return this.instance || (this.instance = new this())
  }
}
