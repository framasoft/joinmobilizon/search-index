/**
 * Taken from https://github.com/Chocobozzz/PeerTube/blob/3329325a64c21067a598ea42a5c7eb0c489aedea/server/lib/schedulers/geo-ip-update-scheduler.ts
 * AGPL
 */

import { GeoIP } from '../../helpers/geo-ip'
import { SCHEDULER_INTERVALS_MS } from '../../initializers/constants'
import { AbstractScheduler } from './abstract-scheduler'

export class GeoIPUpdateScheduler extends AbstractScheduler {
  private static instance: AbstractScheduler

  protected schedulerIntervalMs = SCHEDULER_INTERVALS_MS.GEO_IP_UPDATE

  private constructor() {
    super()
  }

  protected internalExecute() {
    return GeoIP.Instance.updateDatabase()
  }

  static get Instance() {
    return this.instance || (this.instance = new this())
  }
}
