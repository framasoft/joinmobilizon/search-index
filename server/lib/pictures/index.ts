import { search as unsplashSearch } from './providers/unsplash'
import { search as flickrSearch } from './providers/flickr'
import { CONFIG } from '../../initializers/constants'
import { PictureInformation } from '../../../shared/picture'

export async function search(location: string): Promise<PictureInformation> {
  switch (CONFIG.PICTURES.TYPE) {
    case 'unsplash':
      return await unsplashSearch(location, {
        endpoint: CONFIG.PICTURES.ENDPOINT,
        accessKey: CONFIG.PICTURES.ACCESS_KEY,
      })
    case 'flickr':
      return await flickrSearch(location, {
        endpoint: CONFIG.PICTURES.ENDPOINT,
        accessKey: CONFIG.PICTURES.ACCESS_KEY,
      })
    default:
      throw new Error('No picture provider registered')
  }
}
