import { ProviderOptions } from '../../../types/geocoding'
import { logger } from '../../../helpers/logger'
import { sample } from 'lodash'
import { UnsplashSearchPhotosResponse } from '../../../types/pictures/unsplash'
import { doJSONRequest } from '../../../helpers/requests'
import { PictureInformation } from '../../../../shared/picture'
import { CONFIG } from '../../../initializers/constants'

const UNSPLASH_API = '/search/photos'
const UNSPLASH_NAME = 'Unsplash'
const UNSPLASH_APP_NAME = CONFIG.PICTURES.OPTIONS['app_name']
const UNSPLASH_UTM_SOURCE = `?utm_source=${UNSPLASH_APP_NAME}&utm_medium=referral`
const UNSPLASH_URL = `https://unsplash.com/${UNSPLASH_UTM_SOURCE}`

export async function search(location: string, options: ProviderOptions): Promise<PictureInformation> {
  logger.debug(`Asking unsplash for pictures matching ${location}`)
  const url = `${options.endpoint}${UNSPLASH_API}?query=${location}&orientation=landscape`
  try {
    const res = await doJSONRequest<UnsplashSearchPhotosResponse>(url, {
      headers: { Authorization: `Client-ID ${options.accessKey}` },
    })
    const selectedPicture = sample(res.body.results)
    return {
      url: selectedPicture.urls.small,
      author: {
        name: selectedPicture.user.name,
        url: `${selectedPicture.user.links.html}${UNSPLASH_UTM_SOURCE}`,
      },
      source: {
        name: UNSPLASH_NAME,
        url: UNSPLASH_URL,
      },
    }
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Unsplash')
  }
}
