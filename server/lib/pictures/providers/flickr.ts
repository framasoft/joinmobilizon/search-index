import { doJSONRequest } from '../../../helpers/requests'
import { ProviderOptions } from '../../../types/geocoding'
import { logger } from '../../../helpers/logger'
import { sample } from 'lodash'
import { FlickrGetInfoPeopleResponse, FlickrSearchPhotosResponse } from '../../../types/pictures/flickr'
import { PictureInformation } from '../../../../shared/picture'

const SEARCH_PHOTOS_METHOD = '?method=flickr.photos.search'
const GET_INFO_PEOPLE_METHOD = '?method=flickr.people.getInfo'

export async function search(location: string, options: ProviderOptions): Promise<PictureInformation> {
  logger.debug(`Asking flickr for pictures matching ${location}`)
  const url = `${options.endpoint}${SEARCH_PHOTOS_METHOD}&api_key=${options.accessKey}&text=${location}&license=4,5,7,8,9,10&sort=relevance&format=json&nojsoncallback=1&orientation=landscape`
  console.debug('url', url)
  try {
    const res = await doJSONRequest<FlickrSearchPhotosResponse>(url)
    const photo = sample(res.body.photos.photo)
    const ownerURL = `${options.endpoint}${GET_INFO_PEOPLE_METHOD}&api_key=${options.accessKey}&user_id=${photo.owner}&format=json&nojsoncallback=1`
    const resOwner = await doJSONRequest<FlickrGetInfoPeopleResponse>(ownerURL)
    return {
      url: `https://live.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`,
      author: {
        name: resOwner.body.person.username._content,
        url: resOwner.body.person.profileurl._content,
      },
      source: {
        name: 'Flickr',
        url: 'https://www.flickr.com/',
      },
    }
  } catch (err) {
    logger.error({ err }, 'Failure fetching location details from Flickr')
  }
}
