import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { EventsSearchQuery } from '../../types/search-query/event-search.model'
import { DBEvent, EnhancedEvent, IndexableEvent } from '../../types/event.model'
import { buildSort, extractQueryResult } from './elastic-search-queries'
import {
  addUUIDFilters,
  buildActorSummaryMapping,
  buildMultiMatchBool,
  formatActorForDB,
  formatActorSummaryForAPI,
} from './shared'
import { buildLocationSummaryMapping, formatLocationForDB } from './shared/elastic-search-address'
import { formatEventStatusForDB } from './shared/schema-utils'
import { MappingProperty, PropertyName } from '@elastic/elasticsearch/lib/api/types'
import { SortOrder } from '@elastic/elasticsearch/lib/api/typesWithBodyKey'
import { detectEventLanguage } from './shared/language-detection'
import { proxifyEventThumbnail } from '../proxy/imgproxy'

async function queryEvents(search: EventsSearchQuery) {
  const bool: any = {}
  const filter: any[] = []
  const must: any[] = []
  const mustNot: any[] = []

  let lat: string, lon: string
  if (search.latlon) {
    ;[lat, lon] = search.latlon.split(':')
  }

  if (search.bbox) {
    const [topLeft, bottomRight] = search.bbox.split(':')

    must.push(
      {
        exists: {
          field: 'location.location',
          boost: 1.0,
        },
      },
      {
        geo_bounding_box: {
          'location.location': {
            top_left: topLeft,
            bottom_right: bottomRight,
          },
          validation_method: 'strict',
          // type: 'MEMORY',
          ignore_unmapped: false,
          boost: 1.0,
        },
      }
    )
  }

  if (search.search) {
    Object.assign(bool, buildMultiMatchBool(search.search, ELASTIC_SEARCH_QUERY.EVENTS_MULTI_MATCH_FIELDS))
  }

  if (search.blockedAccounts) {
    mustNot.push({
      terms: {
        'creator.handle': search.blockedAccounts,
      },
    })

    mustNot.push({
      terms: {
        'group.handle': search.blockedAccounts,
      },
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts,
      },
    })
  }

  if (search.startDate) {
    filter.push({
      range: {
        publishedAt: {
          gte: search.startDate,
        },
      },
    })
  }

  if (search.endDate) {
    filter.push({
      range: {
        publishedAt: {
          lte: search.endDate,
        },
      },
    })
  }

  if (search.categoryOneOf) {
    filter.push({
      terms: {
        category: search.categoryOneOf,
      },
    })
  }

  if (search.languageOneOf) {
    filter.push({
      terms: {
        language: search.languageOneOf,
      },
    })
  }

  if (search.tagsOneOf) {
    filter.push({
      terms: {
        tags: search.tagsOneOf,
      },
    })
  }

  if (search.tagsAllOf) {
    for (const t of search.tagsAllOf) {
      filter.push({
        term: {
          tags: t,
        },
      })
    }
  }

  if (search.host) {
    filter.push({
      term: {
        'creator.host': search.host,
      },
    })
  }

  if (search.startDateMin) {
    filter.push({
      range: {
        startTime: {
          gte: search.startDateMin,
        },
      },
    })
  }

  if (search.startDateMax) {
    filter.push({
      range: {
        startTime: {
          lte: search.startDateMax,
        },
      },
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere' && !search.isOnline) {
    filter.push({
      geo_distance: {
        distance: `${search.distance}km`,
        'location.location': {
          lat,
          lon,
        },
      },
    })
  }

  if (search.isOnline) {
    filter.push({
      term: {
        isOnline: true,
      },
    })
  }

  if (search.statusOneOf) {
    filter.push({
      terms: {
        status: search.statusOneOf,
      },
    })
  }

  if (search.uuids) {
    addUUIDFilters(filter, search.uuids)
  }

  Object.assign(bool, { filter })

  Object.assign(bool, { must_not: mustNot, must: [...(bool.must ?? []), ...must] })

  const body = {
    min_score: search.categoryOneOf ? 0 : 0,
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort),
  }

  // Allow boosting results depending on query languages
  if (
    CONFIG.EVENTS_SEARCH.BOOST_LANGUAGES.ENABLED &&
    Array.isArray(search.boostLanguages) &&
    search.boostLanguages.length !== 0
  ) {
    const boostScript = `
      double score = _score;
      float nbParticipants = 0;
      if (doc['participantCount'].size() != 0) {
        nbParticipants = doc['participantCount'].value;
      }
      score = score + (1 / (1 + Math.exp(-nbParticipants * params.participantBoost)));

      if (doc['language'].size() == 0) {
        return score;
      }

      String language = doc['language'].value;

      for (String docLang: params.boostLanguages) {
        if (docLang == language) return score * params.languageBoost;
      }

      return score * params.languageMalus;
    `

    Object.assign(body, {
      query: {
        script_score: {
          query: { bool },
          script: {
            source: boostScript,
            params: {
              boostLanguages: search.boostLanguages,
              languageBoost: ELASTIC_SEARCH_QUERY.BOOST_LANGUAGE_VALUE,
              languageMalus: ELASTIC_SEARCH_QUERY.MALUS_LANGUAGE_VALUE,
              participantBoost: ELASTIC_SEARCH_QUERY.EVENT_PARTICIPANTS_BOOST,
            },
          },
        },
      },
    })
  } else {
    const boostScript = `
      double score = _score;
      float nbParticipants = 0;
      if (doc['participantCount'].size() != 0) {
        nbParticipants = doc['participantCount'].value;
      }
      return score + (1 / (1 + Math.exp(-nbParticipants * params.participantBoost)));
    `

    Object.assign(body, {
      query: {
        script_score: {
          query: { bool },
          script: {
            source: boostScript,
            params: {
              participantBoost: ELASTIC_SEARCH_QUERY.EVENT_PARTICIPANTS_BOOST,
            },
          },
        },
      },
    })

    Object.assign(body, { query: { bool } })
  }

  logger.debug({ body }, 'Will query Elastic Search for events.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS,
    body,
  })

  return extractQueryResult(res)
}

export async function categoryStats(languages: string[]) {
  const body: any = {
    query: {
      bool: {
        filter: [
          {
            range: {
              startTime: {
                gte: new Date().toISOString(),
              },
            },
          },
        ],
      },
    },
    size: 0,
    aggs: {
      category: {
        terms: {
          field: 'category',
          size: 100,
          order: {
            _count: 'desc' as SortOrder,
          },
        },
      },
    },
  }

  if (languages.length > 0) {
    body.query.bool.filter.push({
      terms: {
        language: languages,
      },
    })
  }

  logger.debug({ body }, 'Will query Elastic Search for events stats.')

  const res = await elasticSearch.search<any, any>({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS,
    body,
  })
  return res.aggregations.category.buckets
}

function buildEventsMapping(): Record<PropertyName, MappingProperty> {
  return {
    id: {
      type: 'binary',
    },

    uuid: {
      type: 'keyword',
    },
    shortUUID: {
      type: 'keyword',
    },
    createdAt: {
      type: 'date',
      format: 'date_optional_time',
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time',
    },
    publishedAt: {
      type: 'date',
      format: 'date_optional_time',
    },
    indexedAt: {
      type: 'date',
      format: 'date_optional_time',
    },

    category: {
      type: 'keyword',
    },

    language: {
      type: 'keyword',
    },

    name: {
      type: 'text',
    },

    description: {
      type: 'text',
      analyzer: 'html_analyzer',
    },

    startTime: {
      type: 'date',
      format: 'date_optional_time',
    },

    endTime: {
      type: 'date',
      format: 'date_optional_time',
    },

    tags: {
      type: 'text',

      fields: {
        raw: {
          type: 'keyword',
        },
      },
    },

    url: {
      type: 'keyword',
    },

    host: {
      type: 'keyword',
    },

    status: {
      type: 'keyword',
    },

    isOnline: {
      type: 'boolean',
    },

    creator: {
      properties: buildActorSummaryMapping(),
    },

    group: {
      properties: buildActorSummaryMapping(),
    },

    location: {
      properties: buildLocationSummaryMapping(),
    },
  }
}

function formatEventForDB(e: IndexableEvent): DBEvent {
  const banner = e.attachment.find((a) => a.name === 'Banner')
  return {
    id: e.id,
    uuid: e.uuid,

    indexedAt: new Date(),
    updated: e.updated,
    published: e.published,

    name: e.name,
    // description: e.content,
    endTime: e.endTime,
    startTime: e.startTime,
    status: formatEventStatusForDB(e['ical:status']),
    joinMode: e.joinMode,
    maximumAttendeeCapacity: e.maximumAttendeeCapacity,
    category: e.category,

    host: e.host,
    url: e.url,

    tags: e.tag.map(({ name }: { name: string }) => name),
    location: formatLocationForDB(e.location),
    language: e.inLanguage !== 'und' ? e.inLanguage : undefined || detectEventLanguage(e),

    creator: formatActorForDB(e.actor),
    group: e.attributedTo && e.attributedTo?.id !== e.actor?.id ? formatActorForDB(e.attributedTo) : null,
    banner: banner ? banner.url : null,
    isOnline: e.isOnline,
    remainingAttendeeCapacity: e.remainingAttendeeCapacity,
    participantCount: e.participantCount,
  }
}

function formatEventForAPI(e: DBEvent, fromHost?: string): EnhancedEvent {
  return {
    joinMode: e.joinMode,
    maximumAttendeeCapacity: e.maximumAttendeeCapacity,
    id: e.id,
    uuid: e.uuid,

    score: e.score,
    language: e.language,

    name: e.name,
    endTime: e.endTime,
    startTime: e.startTime,
    status: e.status,
    published: e.published,
    tags: e.tags,
    updated: e.updated,
    location: e.location,

    url: e.url,
    creator: formatActorSummaryForAPI(e.creator),
    group: formatActorSummaryForAPI(e.group),
    banner: proxifyEventThumbnail(e.banner),
    isOnline: e.isOnline,
    remainingAttendeeCapacity: e.remainingAttendeeCapacity,
    category: e.category,
    participantCount: e.participantCount,
  }
}

export { queryEvents, formatEventForDB, formatEventForAPI, buildEventsMapping }
