import { htmlToText } from '../../helpers/sanitizer'
import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { DBGroup, EnhancedGroup, IndexableGroup } from '../../types/group.model'
import { GroupsSearchQuery } from '../../types/search-query/group-search.model'
import { buildSort, extractQueryResult } from './elastic-search-queries'
import { buildActorCommonMapping } from './shared'
import { buildLocationSummaryMapping, formatLocationForDB } from './shared/elastic-search-address'
import { detectActorLanguage } from './shared/language-detection'
import { proxifyGroupThumbnail } from '../proxy/imgproxy'

async function queryGroups(search: GroupsSearchQuery) {
  const bool: any = {}
  const must: any[] = []
  const mustNot: any[] = []
  const filter: any[] = []

  let lat: string, lon: string
  if (search.latlon) {
    ;[lat, lon] = search.latlon.split(':')
  }

  if (search.bbox) {
    const [topLeft, bottomRight] = search.bbox.split(':')

    must.push(
      {
        exists: {
          field: 'location.location',
          boost: 1.0,
        },
      },
      {
        geo_bounding_box: {
          'location.location': {
            top_left: topLeft,
            bottom_right: bottomRight,
          },
          validation_method: 'strict',
          // type: 'MEMORY',
          ignore_unmapped: false,
          boost: 1.0,
        },
      }
    )
  }

  if (search.search) {
    must.push({
      multi_match: {
        query: search.search,
        fields: ELASTIC_SEARCH_QUERY.GROUPS_MULTI_MATCH_FIELDS,
        fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS,
      },
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts,
      },
    })
  }

  if (search.host) {
    filter.push({
      term: {
        host: search.host,
      },
    })
  }

  if (search.handles) {
    filter.push({
      terms: {
        handle: search.handles,
      },
    })
  }

  if (search.languageOneOf) {
    filter.push({
      terms: {
        language: search.languageOneOf,
      },
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere') {
    filter.push({
      geo_distance: {
        distance: `${search.distance}km`,
        'location.location': {
          lat,
          lon,
        },
      },
    })
  }

  if (filter.length !== 0) {
    Object.assign(bool, { filter })
  }

  if (mustNot.length !== 0) {
    Object.assign(bool, { must_not: mustNot, must })
  }

  const body = {
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort),
    query: { bool },
  }

  const boostScript = `
    double score = _score;
    float nbMembers = 0;
    if (doc['memberCount'].size() != 0) {
      nbMembers = doc['memberCount'].value;
    }
    score = score + (1 / (1 + Math.exp(-nbMembers * params.memberBoost)));

    return score * 2;
  `

  Object.assign(body, {
    query: {
      script_score: {
        query: { bool },
        script: {
          source: boostScript,
          params: {
            memberBoost: ELASTIC_SEARCH_QUERY.GROUP_MEMBERS_BOOST,
          },
        },
      },
    },
  })

  logger.debug({ body }, 'Will query Elastic Search for groups.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.GROUPS,
    body,
  })

  return extractQueryResult(res)
}

function formatGroupForAPI(c: DBGroup, fromHost?: string): EnhancedGroup {
  return {
    id: c.id,

    score: c.score,

    // url: c.id,
    name: c.name,
    // host: c.host,
    avatar: proxifyGroupThumbnail(c.avatar),

    displayName: c.displayName,
    description: c.description,
    url: c.id,
    location: c.location,
    host: c.host,
    memberCount: c.memberCount,
    openness: c.openness,
    manuallyApprovesFollowers: c.manuallyApprovesFollowers,
    language: c.language,
  }
}

function formatGroupForDB(c: IndexableGroup): DBGroup {
  return {
    id: c.id,

    name: c.preferredUsername,
    host: c.host,
    url: c.url,

    avatar: c.icon?.url,

    displayName: c.name,

    indexedAt: new Date(),
    description: htmlToText(c.summary),
    location: formatLocationForDB(c.location),

    handle: c.handle,
    memberCount: c.memberCount,
    openness: c.openness,
    manuallyApprovesFollowers: c.manuallyApprovesFollowers,
    language: detectActorLanguage(c),
  }
}

function buildGroupsMapping() {
  const base = buildActorCommonMapping()

  return {
    ...base,
    location: {
      properties: buildLocationSummaryMapping(),
    },
  }
}

export { buildGroupsMapping, formatGroupForDB, formatGroupForAPI, queryGroups }
