import { DeleteByQueryResponse, SearchResponse } from '@elastic/elasticsearch/lib/api/types'
import { difference } from 'lodash'
import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { MarkerAggregate } from '../../types/event-marker.model'

async function removeNotExistingIdsFromHost(indexName: string, host: string, existingIds: Set<number>) {
  const idsFromDB = await getIdsOf(indexName, host)

  const idsToRemove = difference(idsFromDB, Array.from(existingIds))

  logger.info({ idsToRemove }, 'Will remove %d entries from %s of host %s.', idsToRemove.length, indexName, host)

  return elasticSearch.deleteByQuery({
    index: indexName,
    body: {
      query: {
        bool: {
          filter: [
            {
              terms: {
                id: idsToRemove,
              },
            },
            {
              term: {
                host,
              },
            },
          ],
        },
      },
    },
  })
}

function removeFromHosts(indexName: string, hosts: string[]): Promise<DeleteByQueryResponse> {
  if (hosts.length === 0) return

  logger.info({ hosts }, 'Will remove entries of index %s from hosts.', indexName)

  return elasticSearch.deleteByQuery({
    index: indexName,
    body: {
      query: {
        bool: {
          filter: {
            terms: {
              host: hosts,
            },
          },
        },
      },
    },
  })
}

async function getIdsOf(indexName: string, host: string) {
  const res = await elasticSearch.search<any, any>({
    index: indexName,
    body: {
      size: 0,
      aggs: {
        ids: {
          terms: {
            size: 500000,
            field: 'id',
          },
        },
      },
      query: {
        bool: {
          filter: [
            {
              term: {
                host,
              },
            },
          ],
        },
      },
    },
  })

  return res.aggregations.ids.buckets.map((b) => b.key)
}

function extractQueryResult(result: SearchResponse<any, any>) {
  const hits = result.hits

  return {
    total: typeof hits.total === 'number' ? hits.total : hits.total.value,
    data: hits.hits.map((h) => Object.assign(h._source, { score: h._score })),
  }
}

function extractEventMarkersQueryResult(result: SearchResponse<any, any>) {
  const { hits, aggregations } = result
  const mappedHits = hits.hits.map((h) => Object.assign(h._source, { score: h._score }))
  const buckets = aggregations?.gridSplit?.buckets as MarkerAggregate[]
  const totalNumberOfAggregates = buckets.reduce((acc, b) => acc + b.doc_count, 0)
  let data
  if (totalNumberOfAggregates < 5 || buckets.length <= 1) {
    data = mappedHits
  } else {
    data = buckets
  }

  return {
    total: typeof hits.total === 'number' ? hits.total : hits.total.value,
    data,
    aggregations,
  }
}

function buildSort(value: string) {
  let sortField: string
  let direction: 'asc' | 'desc'

  if (value.substring(0, 1) === '-') {
    direction = 'desc'
    sortField = value.substring(1)
  } else {
    direction = 'asc'
    sortField = value
  }

  const field = sortField === 'match' ? '_score' : sortField

  return [
    {
      [field]: { order: direction },
    },
  ]
}

export {
  elasticSearch,
  removeNotExistingIdsFromHost,
  getIdsOf,
  extractQueryResult,
  extractEventMarkersQueryResult,
  removeFromHosts,
  buildSort,
}
