import { elasticSearch } from '../../helpers/elastic-search'
import { CONFIG } from '../../initializers/constants'
import { listIndexInstancesHost } from '../requests/instances-index'
import { Instance } from '../../types/instance.model'
import {
  AggregationsBuckets,
  AggregationsMultiBucketAggregateBase,
  AggregationsStringRareTermsBucketKeys,
} from '@elastic/elasticsearch/lib/api/types'

async function buildInstanceHosts() {
  let indexHosts = await listIndexInstancesHost()

  if (CONFIG.INSTANCES_INDEX.WHITELIST.ENABLED) {
    const whitelistHosts = Array.isArray(CONFIG.INSTANCES_INDEX.WHITELIST.HOSTS)
      ? CONFIG.INSTANCES_INDEX.WHITELIST.HOSTS
      : []

    indexHosts = indexHosts.filter((h) => whitelistHosts.includes(h))
  }

  if (CONFIG.INSTANCES_INDEX.BLOCKLIST.ENABLED) {
    const blocklistHosts = Array.isArray(CONFIG.INSTANCES_INDEX.BLOCKLIST.HOSTS)
      ? CONFIG.INSTANCES_INDEX.BLOCKLIST.HOSTS
      : []

    indexHosts = indexHosts.filter((h) => !blocklistHosts.includes(h))
  }

  const dbHosts = await listDBInstances()
  const removedHosts = getRemovedHosts(dbHosts, indexHosts)

  return { indexHosts, removedHosts }
}

export { buildInstanceHosts }

// ##################################################

async function listDBInstances() {
  const setResult = new Set<string>()
  const indexes = [CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS, CONFIG.ELASTIC_SEARCH.INDEXES.GROUPS]

  for (const index of indexes) {
    const res = await elasticSearch.search<
      Instance,
      Record<'hosts', AggregationsMultiBucketAggregateBase<AggregationsBuckets<AggregationsStringRareTermsBucketKeys>>>
    >({
      index,
      body: {
        size: 0,
        aggs: {
          hosts: {
            terms: {
              size: 5000,
              field: 'host',
            },
          },
        },
      },
    })

    // @ts-ignore
    for (const b of res.aggregations.hosts.buckets) {
      setResult.add(b.key)
    }
  }

  return Array.from(setResult)
}

function getRemovedHosts(dbHosts: string[], indexHosts: string[]): string[] {
  return dbHosts.filter((dbHost) => indexHosts.includes(dbHost) === false)
}
