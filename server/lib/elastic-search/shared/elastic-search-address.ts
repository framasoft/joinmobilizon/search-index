import { MappingProperty, PropertyName } from '@elastic/elasticsearch/lib/api/types'
import { APAddress, APLocation, DBAddress, DBLocation } from 'server/types/address.model'

function buildLocationSummaryMapping(): Record<PropertyName, MappingProperty> {
  return {
    id: {
      type: 'binary',
    },

    name: {
      type: 'text',
      fields: {
        raw: {
          type: 'keyword',
        },
      },
    },
    location: {
      type: 'geo_point',
    },
    address: {
      properties: {
        addressCountry: {
          type: 'keyword',
        },
        addressLocality: {
          type: 'keyword',
        },
        addressRegion: {
          type: 'keyword',
        },
        postalCode: {
          type: 'binary',
        },
        streetAddress: {
          type: 'binary',
        },
      },
    },
  }
}

function formatLocationForDB(location: APLocation): DBLocation {
  if (location?.id && (location.name || (location.latitude && location.latitude))) {
    return {
      address: formatAddressForDB(location.address),
      id: location.id,
      location: { lat: location.latitude, lon: location.longitude },
      name: location.name,
    }
  }
}

function formatAddressForDB(address: APAddress): DBAddress {
  return {
    addressCountry: address.addressCountry,
    addressLocality: address.addressLocality,
    addressRegion: address.addressRegion,
    postalCode: address.postalCode,
    streetAddress: address.streetAddress,
  }
}

export { buildLocationSummaryMapping, formatAddressForDB, formatLocationForDB }
