import { MappingProperty, PropertyName } from '@elastic/elasticsearch/lib/api/types'
import { Actor, APActor, DBActor } from 'server/types/actor.model'
import { proxifyAuthorThumbnail } from '../../proxy/imgproxy'
import { detectActorLanguage } from './language-detection'

function buildActorSummaryMapping(): Record<PropertyName, MappingProperty> {
  return {
    id: {
      type: 'binary',
    },

    name: {
      type: 'text',
      fields: {
        raw: {
          type: 'keyword',
        },
      },
    },
    displayName: {
      type: 'text',
    },
    url: {
      type: 'keyword',
    },
    host: {
      type: 'keyword',
    },
    handle: {
      type: 'keyword',
    },
    avatar: {
      type: 'binary',
    },
    language: {
      type: 'keyword',
    },
  }
}

function buildActorCommonMapping(): Record<PropertyName, MappingProperty> {
  return {
    ...buildActorSummaryMapping(),

    createdAt: {
      type: 'date',
      format: 'date_optional_time',
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time',
    },

    description: {
      type: 'text',
    },
  }
}

function formatActorSummaryForAPI(actor: Actor): Omit<DBActor, 'handle'> | null {
  if (!actor) return null
  const url = new URL(actor.id)
  return {
    id: actor.id,
    name: actor.name,
    displayName: actor.displayName,
    url: actor.id,
    host: url.host,
    description: actor.description,
    avatar: proxifyAuthorThumbnail(actor.avatar),
    openness: actor.openness,
    manuallyApprovesFollowers: actor.manuallyApprovesFollowers,
    language: actor.language,
  }
}

function formatActorForDB(actor: APActor): DBActor {
  if (actor?.id) {
    const url = new URL(actor.id)
    return {
      id: actor.id,
      name: actor.preferredUsername,
      displayName: actor.name,
      url: actor.id,
      host: url.host,
      description: actor.summary,

      handle: `${actor.preferredUsername}@${url.host}`,

      avatar: actor.icon?.url,
      openness: actor.openness,
      manuallyApprovesFollowers: actor.manuallyApprovesFollowers,
      language: detectActorLanguage(actor),
    }
  }
}

export { buildActorCommonMapping, buildActorSummaryMapping, formatActorSummaryForAPI, formatActorForDB }
