import LanguageDetect from 'languagedetect'
import { APEvent } from 'server/types/event.model'
import { stripTags } from '../../../helpers/sanitizer'
import { APActor } from '../../../types/actor.model'

export function detectActorLanguage(actor: APActor): string | null {
  if (!actor?.name || !actor?.summary) return null
  return detectTextLanguage(`${actor.name}\n${stripTags(actor.summary)}`)
}

export function detectEventLanguage(event: APEvent): string | null {
  if (!event?.name || !event?.content) return null
  return detectTextLanguage(`${event.name}\n${stripTags(event.content)}`)
}

export function detectTextLanguage(text: string) {
  const lngDetector = new LanguageDetect()
  lngDetector.setLanguageType('iso2')
  const result = lngDetector.detect(text, 1)
  if (result.length > 0) {
    const [[language, trust]] = result

    if (trust > 0.3) {
      return language
    }
  }
  return null
}
