import { EventStatus } from '../../../types/event.model'

function formatEventStatusForDB(status: string): EventStatus | undefined {
  switch (status) {
    case EventStatus.TENTATIVE:
      return EventStatus.TENTATIVE
    case EventStatus.CONFIRMED:
      return EventStatus.CONFIRMED
    case EventStatus.CANCELLED:
      return EventStatus.CANCELLED
    default:
      return undefined
  }
}

export { formatEventStatusForDB }
