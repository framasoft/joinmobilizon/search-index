import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { extractEventMarkersQueryResult } from './elastic-search-queries'
import { EventMarkersSearchQuery } from '../../types/search-query/event-search.model'
import { addUUIDFilters } from './shared'
import { DBEvent } from '../../types/event.model'
import { MarkerAggregate } from '../../types/event-marker.model'
import { Feature, Point } from 'geojson'
import { formatEventForAPI } from './elastic-search-events'
import { QueryDslBoolQuery, QueryDslOperator, QueryDslQueryContainer } from '@elastic/elasticsearch/lib/api/types'

async function queryEventMarkers(search: EventMarkersSearchQuery) {
  const [topLeft, bottomRight] = search.bbox.split(':')

  const bool: QueryDslBoolQuery = {}
  const filter: QueryDslQueryContainer[] = []
  const mustNot: QueryDslQueryContainer[] = []
  const must: QueryDslQueryContainer[] = []

  must.push(
    {
      exists: {
        field: 'location.location',
        boost: 1.0,
      },
    },
    {
      geo_bounding_box: {
        'location.location': {
          top_left: topLeft,
          bottom_right: bottomRight,
        },
        validation_method: 'strict',
        // type: 'MEMORY',
        ignore_unmapped: false,
        boost: 1.0,
      },
    }
  )

  let lat: string, lon: string
  if (search.latlon) {
    ;[lat, lon] = search.latlon.split(':')
  }

  if (search.search) {
    must.push({
      multi_match: {
        query: search.search,
        fields: ELASTIC_SEARCH_QUERY.EVENTS_MULTI_MATCH_FIELDS,
        fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS,
        operator: ELASTIC_SEARCH_QUERY.OPERATOR as QueryDslOperator,
        minimum_should_match: ELASTIC_SEARCH_QUERY.MINIMUM_SHOULD_MATCH,
      },
    })
  }

  if (search.blockedAccounts) {
    mustNot.push({
      terms: {
        'creator.handle': search.blockedAccounts,
      },
    })

    mustNot.push({
      terms: {
        'group.handle': search.blockedAccounts,
      },
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts,
      },
    })
  }

  if (search.categoryOneOf) {
    filter.push({
      terms: {
        category: search.categoryOneOf,
      },
    })
  }

  if (search.languageOneOf) {
    filter.push({
      terms: {
        language: search.languageOneOf,
      },
    })
  }

  if (search.tagsOneOf) {
    filter.push({
      terms: {
        tags: search.tagsOneOf,
      },
    })
  }

  if (search.tagsAllOf) {
    for (const t of search.tagsAllOf) {
      filter.push({
        term: {
          tags: t,
        },
      })
    }
  }

  if (search.host) {
    filter.push({
      term: {
        'creator.host': search.host,
      },
    })
  }

  if (search.startDateMin) {
    must.push({
      range: {
        startTime: {
          gte: search.startDateMin,
        },
      },
    })
  }

  if (search.startDateMax) {
    must.push({
      range: {
        startTime: {
          lte: search.startDateMax,
        },
      },
    })
  }

  if (search.isOnline === true) {
    must.push({
      term: {
        isOnline: true,
      },
    })
  }

  if (search.statusOneOf) {
    must.push({
      terms: {
        status: search.statusOneOf,
      },
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere') {
    must.push({
      geo_distance: {
        distance: search.distance,
        'location.location': {
          lat: parseFloat(lat),
          lon: parseFloat(lon),
        },
      },
    })
  }

  if (search.uuids) {
    addUUIDFilters(filter, search.uuids)
  }

  bool.must_not = mustNot
  bool.must = must
  bool.filter = filter

  const body = {
    from: search.start,
    size: search.count,
  }

  Object.assign(body, {
    size: 10,
    aggregations: {
      gridSplit: {
        geotile_grid: {
          field: 'location.location',
          precision: 9,
          bounds: {
            top_left: topLeft,
            bottom_right: bottomRight,
          },
        },
        aggregations: {
          gridCentroid: {
            geo_centroid: {
              field: 'location.location',
            },
          },
        },
      },
    },
  })

  // Allow boosting results depending on query languages
  // if (
  //   CONFIG.EVENTS_SEARCH.BOOST_LANGUAGES.ENABLED &&
  //   Array.isArray(search.boostLanguages) &&
  //   search.boostLanguages.length !== 0
  // ) {
  //   const boostScript = `
  //   if (doc['language'].size() == 0) {
  //     return _score;
  //   }
  //
  //   String language = doc['language'].value;
  //
  //   for (String docLang: params.boostLanguages) {
  //     if (docLang == language) return _score * params.boost;
  //   }
  //
  //   return _score;
  // `
  //
  //   Object.assign(body, {
  //     query: {
  //       script_score: {
  //         query: { bool },
  //         script: {
  //           source: boostScript,
  //           params: {
  //             boostLanguages: search.boostLanguages,
  //             boost: ELASTIC_SEARCH_QUERY.BOOST_LANGUAGE_VALUE
  //           }
  //         }
  //       }
  //     }
  //   })
  // } else {
  Object.assign(body, { query: { bool } })
  // }

  logger.debug({ body }, 'Will query Elastic Search for event markers.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.EVENTS,
    body,
  })
  return extractEventMarkersQueryResult(res)
}

function formatEventMakerForAPI(e: DBEvent | MarkerAggregate, fromHost?: string): Feature<Point> {
  if ('doc_count' in e) {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [e.gridCentroid.location.lon, e.gridCentroid.location.lat],
      },
      properties: {
        key: e.key,
        point_count: e.doc_count,
      },
    }
  }
  if (e.location && e.location.location.lon) {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [e.location.location.lon, e.location.location.lat],
      },
      properties: {
        ...formatEventForAPI(e),
      },
    } as Feature<Point>
  }
}

export { queryEventMarkers, formatEventMakerForAPI }
