import { elasticSearch } from '../../helpers/elastic-search'
import { logger } from '../../helpers/logger'
import { CONFIG, ELASTIC_SEARCH_QUERY } from '../../initializers/constants'
import { buildSort, extractEventMarkersQueryResult } from './elastic-search-queries'
import { MarkerAggregate } from '../../types/event-marker.model'
import { Feature, Point } from 'geojson'
import { GroupMarkersSearchQuery } from '../../types/search-query/group-search.model'
import { DBGroup } from '../../types/group.model'
import { formatGroupForAPI } from './elastic-search-groups'

async function queryGroupMarkers(search: GroupMarkersSearchQuery) {
  const bool: any = {}
  const must: any[] = []
  const mustNot: any[] = []
  const filter: any[] = []

  const [topLeft, bottomRight] = search.bbox.split(':')

  let lat, lon
  if (search.latlon) {
    ;[lat, lon] = search.latlon.split(':')
  }

  if (search.search) {
    Object.assign(bool, {
      must: [
        {
          multi_match: {
            query: search.search,
            fields: ELASTIC_SEARCH_QUERY.GROUPS_MULTI_MATCH_FIELDS,
            fuzziness: ELASTIC_SEARCH_QUERY.FUZZINESS,
          },
        },
      ],
    })
  }

  if (search.blockedHosts) {
    mustNot.push({
      terms: {
        host: search.blockedHosts,
      },
    })
  }

  if (search.host) {
    filter.push({
      term: {
        host: search.host,
      },
    })
  }

  if (search.handles) {
    filter.push({
      terms: {
        handle: search.handles,
      },
    })
  }

  if (lat && lon && search.distance && search.distance !== 'anywhere') {
    must.push({
      geo_distance: {
        distance: search.distance,
        'location.location': {
          lat,
          lon,
        },
      },
    })
  }

  if (mustNot.length !== 0) {
    Object.assign(filter, { must_not: mustNot, must })
  }

  if (filter.length !== 0) {
    Object.assign(bool, { filter })
  }

  const body = {
    from: search.start,
    size: search.count,
    sort: buildSort(search.sort),
    query: { bool },
  }

  Object.assign(body, {
    size: 10,
    aggregations: {
      gridSplit: {
        geotile_grid: {
          field: 'location.location',
          precision: 9,
          bounds: {
            top_left: topLeft,
            bottom_right: bottomRight,
          },
        },
        aggregations: {
          gridCentroid: {
            geo_centroid: {
              field: 'location.location',
            },
          },
        },
      },
    },
  })

  logger.debug({ body }, 'Will query Elastic Search for groups.')

  const res = await elasticSearch.search({
    index: CONFIG.ELASTIC_SEARCH.INDEXES.GROUPS,
    body,
  })
  return extractEventMarkersQueryResult(res)
}

function formatGroupMarkerForAPI(e: DBGroup | MarkerAggregate, fromHost?: string): Feature<Point> {
  if ('doc_count' in e) {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [e.gridCentroid.location.lon, e.gridCentroid.location.lat],
      },
      properties: {
        key: e.key,
        point_count: e.doc_count,
      },
    }
  }
  if (e.location && e.location.location.lon) {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [e.location.location.lon, e.location.location.lat],
      },
      properties: {
        ...formatGroupForAPI(e),
      },
    } as Feature<Point>
  }
}

export { queryGroupMarkers, formatGroupMarkerForAPI }
