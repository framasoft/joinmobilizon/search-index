import { Request, Response, NextFunction } from 'express'

const setDefaultSort = setDefaultSortFactory('-createdAt')

const setDefaultSearchSort = setDefaultSortFactory('-match')

// ---------------------------------------------------------------------------

export { setDefaultSort, setDefaultSearchSort }

// ---------------------------------------------------------------------------

function setDefaultSortFactory(sort: string) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.query.sort) req.query.sort = sort

    return next()
  }
}
