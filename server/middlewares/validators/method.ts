import { Request, Response, NextFunction } from 'express'

const methodsValidator = (methods: string[]) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (methods.includes(req.method) !== true) {
      return res.sendStatus(405)
    }

    return next()
  }
}

// ---------------------------------------------------------------------------

export { methodsValidator }
