import config from 'config'
import { join } from 'path'
import { isTestInstance } from '../helpers/core-utils'

const API_VERSION = 'v1'
const SEARCH_ENGINE_VERSION: string = require(join('../../../', 'package.json')).version

const REQUEST_TIMEOUTS = {
  DEFAULT: 7000, // 7 seconds
  FILE: 30000, // 30 seconds
}

const CONFIG = {
  LISTEN: {
    PORT: config.get<number>('listen.port'),
  },
  WEBSERVER: {
    SCHEME: config.get<boolean>('webserver.https') === true ? 'https' : 'http',
    HOSTNAME: config.get<string>('webserver.hostname'),
    PORT: config.get<number>('webserver.port'),
  },
  ELASTIC_SEARCH: {
    HTTP: config.get<string>('elastic-search.http'),
    AUTH: {
      USERNAME: config.get<string>('elastic-search.auth.username'),
      PASSWORD: config.get<string>('elastic-search.auth.password'),
    },
    SSL: {
      CA: config.get<string>('elastic-search.ssl.ca'),
    },
    HOSTNAME: config.get<string>('elastic-search.hostname'),
    PORT: config.get<number>('elastic-search.port'),
    INDEXES: {
      EVENTS: config.get<string>('elastic-search.indexes.events'),
      GROUPS: config.get<string>('elastic-search.indexes.groups'),
      POSTS: config.get<string>('elastic-search.indexes.posts'),
    },
  },
  LOG: {
    LEVEL: config.get<string>('log.level'),
  },
  SEARCH_INSTANCE: {
    NAME: config.get<string>('search-instance.name'),
    NAME_IMAGE: config.get<string>('search-instance.name_image'),
    SEARCH_IMAGE: config.get<string>('search-instance.search_image'),
    DESCRIPTION: config.get<string>('search-instance.description'),
    LEGAL_NOTICES_URL: config.get<string>('search-instance.legal_notices_url'),
    THEME: config.get<string>('search-instance.theme'),
  },
  EVENTS_SEARCH: {
    BOOST_LANGUAGES: {
      ENABLED: config.get<boolean>('events-search.boost-languages.enabled'),
    },
    SEARCH_FIELDS: {
      NAME: {
        FIELD_NAME: 'name',
        BOOST: config.get<number>('events-search.search-fields.name.boost'),
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('events-search.search-fields.description.boost'),
      },
      TAGS: {
        FIELD_NAME: 'tags',
        BOOST: config.get<number>('events-search.search-fields.tags.boost'),
      },
      ACCOUNT_DISPLAY_NAME: {
        FIELD_NAME: 'account.displayName',
        BOOST: config.get<number>('events-search.search-fields.account-display-name.boost'),
      },
    },
  },
  GROUPS_SEARCH: {
    SEARCH_FIELDS: {
      NAME: {
        FIELD_NAME: 'name',
        BOOST: config.get<number>('groups-search.search-fields.name.boost'),
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('groups-search.search-fields.description.boost'),
      },
      DISPLAY_NAME: {
        FIELD_NAME: 'displayName',
        BOOST: config.get<number>('groups-search.search-fields.display-name.boost'),
      },
      ACCOUNT_DISPLAY_NAME: {
        FIELD_NAME: 'ownerAccount.displayName',
        BOOST: config.get<number>('groups-search.search-fields.account-display-name.boost'),
      },
    },
  },
  POSTS_SEARCH: {
    SEARCH_FIELDS: {
      DISPLAY_NAME: {
        FIELD_NAME: 'displayName',
        BOOST: config.get<number>('posts-search.search-fields.display-name.boost'),
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('posts-search.search-fields.description.boost'),
      },
    },
  },
  INSTANCES_INDEX: {
    URL: config.get<string>('instances-index.url'),
    PUBLIC_URL: config.get<string>('instances-index.public_url'),
    WHITELIST: {
      ENABLED: config.get<boolean>('instances-index.whitelist.enabled'),
      HOSTS: config.get<string[]>('instances-index.whitelist.hosts'),
    },
    BLOCKLIST: {
      ENABLED: config.get<boolean>('instances-index.blocklist.enabled'),
      HOSTS: config.get<string[]>('instances-index.blocklist.hosts'),
    },
  },
  API: {
    BLACKLIST: {
      ENABLED: config.get<boolean>('api.blacklist.enabled'),
      HOSTS: config.get<string[]>('api.blacklist.hosts'),
    },
  },
  GEOCODING: {
    TYPE: config.get<string>('geocoding.type'),
    ENDPOINT: config.get<string>('geocoding.endpoint'),
  },
  PICTURES: {
    TYPE: config.get<string>('pictures.type'),
    ENDPOINT: config.get<string>('pictures.endpoint'),
    ACCESS_KEY: config.get<string>('pictures.access_key'),
    OPTIONS: config.get<Record<string, string>>('pictures.options'),
  },
  STORAGE: {
    GEO_IP_DIR: config.get<string>('storage.geo_ip'),
  },
  GEO_IP: {
    ENABLED: config.get<boolean>('geo_ip.enabled'),
    COUNTRY: {
      DATABASE_URL: config.get<string>('geo_ip.country.database_url'),
    },
  },
  IMG_PROXY: {
    ENABLED: config.get<boolean>('img_proxy.enabled'),
    BASE_URL: config.get<string>('img_proxy.base_url'),
    KEY: config.get<string>('img_proxy.key'),
    SALT: config.get<string>('img_proxy.salt'),
  },
}

const SORTABLE_COLUMNS = {
  EVENTS_SEARCH: ['match', 'startTime', 'createdAt', 'participantCount'],
  GROUPS_SEARCH: ['match', 'displayName', 'createdAt', 'memberCount'],
  POSTS_SEARCH: ['match', 'createdAt'],
}

const PAGINATION_START = {
  MAX: 9000,
}

const PAGINATION_COUNT = {
  DEFAULT: 20,
  MAX: 500,
}

const SCHEDULER_INTERVALS_MS = {
  indexation: 60000 * 60 * 24, // 24 hours
  GEO_IP_UPDATE: 60000 * 60 * 24 * 3, // 3 days
}

const INDEXER_COUNT = 10
const INDEXER_LIMIT = 500000

const INDEXER_HOST_CONCURRENCY = 3
const INDEXER_QUEUE_CONCURRENCY = 3

const REQUESTS = {
  MAX_RETRIES: 10,
  WAIT: 20000, // 10 seconds
}

const ELASTIC_SEARCH_QUERY = {
  FUZZINESS: 'AUTO:4,7',
  OPERATOR: 'OR',
  MINIMUM_SHOULD_MATCH: '3<75%',
  BOOST_LANGUAGE_VALUE: 1,
  MALUS_LANGUAGE_VALUE: 0.5,
  EVENT_PARTICIPANTS_BOOST: 1,
  GROUP_MEMBERS_BOOST: 0.1,
  EVENTS_MULTI_MATCH_FIELDS: buildMultiMatchFields(CONFIG.EVENTS_SEARCH.SEARCH_FIELDS),
  GROUPS_MULTI_MATCH_FIELDS: buildMultiMatchFields(CONFIG.GROUPS_SEARCH.SEARCH_FIELDS),
  POSTS_MULTI_MATCH_FIELDS: buildMultiMatchFields(CONFIG.POSTS_SEARCH.SEARCH_FIELDS),
}

function getWebserverUrl() {
  if (CONFIG.WEBSERVER.PORT === 80 || CONFIG.WEBSERVER.PORT === 443) {
    return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME
  }

  return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME + ':' + CONFIG.WEBSERVER.PORT
}

function buildMultiMatchFields(fields: { [name: string]: { BOOST: number; FIELD_NAME: string } }) {
  return Object.keys(fields)
    .map((id) => {
      const obj = fields[id]
      if (obj.BOOST <= 0) return ''

      return `${obj.FIELD_NAME}^${obj.BOOST}`
    })
    .filter((v) => !!v)
}

if (isTestInstance()) {
  SCHEDULER_INTERVALS_MS.indexation = 1000 * 60 * 5 // 5 minutes
}

const BINARY_CONTENT_TYPES = new Set(['binary/octet-stream', 'application/octet-stream', 'application/x-binary'])

const ACTIVITY_PUB = {
  POTENTIAL_ACCEPT_HEADERS: [
    'application/activity+json',
    'application/ld+json',
    'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
  ],
  ACCEPT_HEADER: 'application/activity+json, application/ld+json',
  PUBLIC: 'https://www.w3.org/ns/activitystreams#Public',
  COLLECTION_ITEMS_PER_PAGE: 10,
  FETCH_PAGE_LIMIT: 2000,
}

export {
  getWebserverUrl,
  CONFIG,
  API_VERSION,
  SEARCH_ENGINE_VERSION,
  REQUEST_TIMEOUTS,
  BINARY_CONTENT_TYPES,
  ACTIVITY_PUB,
  PAGINATION_COUNT,
  PAGINATION_START,
  SORTABLE_COLUMNS,
  INDEXER_QUEUE_CONCURRENCY,
  SCHEDULER_INTERVALS_MS,
  INDEXER_HOST_CONCURRENCY,
  INDEXER_COUNT,
  INDEXER_LIMIT,
  REQUESTS,
  ELASTIC_SEARCH_QUERY,
}
