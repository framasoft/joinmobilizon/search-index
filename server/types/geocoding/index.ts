export interface ProviderOptions {
  endpoint: string
  userAgent?: string
  accessKey?: string
}
