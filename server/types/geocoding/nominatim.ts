import { Point } from 'geojson'

export interface NominatimSearchResultPlace {
  localname: string
  place_id: number | null
  osm_id: number | null
  osm_type: string | null
  place_type: string | null
  class: string
  type: string
  admin_level: number | null
  rank_address: number
  distance: number
  isaddress: boolean
}

interface NominatimAddressDetails {
  shop?: string
  house_number?: string
  road?: string
  neighbourhood?: string
  suburb?: string
  city?: string
  municipality?: string
  county?: string
  state?: string
  region?: string
  postcode?: string
  country?: string
  country_code?: string
}

export interface BasicNominatimSearchResult {
  place_id: number
  licence?: string
  osm_type: string
  osm_id: number
  boundingbox: string[]
  lat: string
  lon: string
  display_name: string
  place_rank: number
  category: string
  type: string
  importance: number
  icon: string
  address: NominatimAddressDetails
}

export interface NominatimSearchResult {
  place_id: number
  parent_place_id: number
  osm_type: string
  osm_id: number
  category: string
  type: string
  admin_level: number
  localname: string
  names: Record<string, string>
  addresstags: {
    country: string
  }
  housenumber: null
  calculated_postcode: null
  country_code: string
  indexed_date: string
  importance: number
  calculated_importance: number
  extratags: Record<string, string>
  calculated_wikipedia: string
  icon: string
  rank_address: number
  rank_search: number
  isarea: boolean
  centroid: Point
  geometry: Point
  address: NominatimSearchResultPlace[]
  linked_places: NominatimSearchResultPlace[]
}
