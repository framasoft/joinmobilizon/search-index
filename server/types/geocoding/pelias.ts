import { FeatureCollection } from 'geojson'

export interface PeliasReverseGeoCodingResult {
  geocoding: Record<string, any>
  features: FeatureCollection
  bbox: Record<number, number>
}
