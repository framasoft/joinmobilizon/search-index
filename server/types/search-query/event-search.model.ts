import { CommonSearch, CommonQuery } from './common-search.model'
import { EventStatus } from '../event.model'

export interface BasicEventsSearchQuery extends CommonQuery {
  search?: string

  host?: string

  startDate?: string // ISO 8601
  endDate?: string // ISO 8601

  startDateMin?: string // ISO 8601
  startDateMax?: string // ISO 8601

  latlon?: string
  distance?: string

  // UUIDs or short
  uuids?: string[]

  isOnline?: boolean
  statusOneOf?: EventStatus[]

  bbox?: string
  zoom?: number
}

export type EventsSearchQuery = BasicEventsSearchQuery & CommonSearch & { boostLanguages: string[] }

export type EventMarkersSearchQuery = EventsSearchQuery & {
  bbox: string
}
