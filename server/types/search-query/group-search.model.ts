import { CommonQuery, CommonSearch } from './common-search.model'

export interface BasicGroupsSearchQuery extends CommonQuery {
  search?: string

  start?: number
  count?: number
  sort?: string

  latlon?: string
  distance?: string

  host?: string
  handles?: string[]
  bbox?: string
  zoom?: number
}

export type GroupsSearchQuery = BasicGroupsSearchQuery & CommonSearch

export type GroupMarkersSearchQuery = GroupsSearchQuery & {
  bbox: string
}
