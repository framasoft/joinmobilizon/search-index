import { CommonSearch } from './common-search.model'

export interface BasicPostsSearchQuery {
  search?: string

  start?: number
  count?: number
  sort?: string

  host?: string
  uuids?: string[]
}

export type PostsSearchQuery = BasicPostsSearchQuery & CommonSearch
