import { Position } from 'geojson'

export interface GeoIPResponse {
  country: {
    isoCode: string
    name: string
  }
  city?: string
  centroid?: Position // lon, lat
  location?: any
}
