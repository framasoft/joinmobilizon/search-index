export type booleanNumber = 0 | 1

export interface FlickrSearchPhotosResponse {
  photos: {
    page: number
    pages: number
    perpage: number
    total: number
    photo: {
      id: string
      owner: string
      secret: string
      server: string
      farm: number
      title: string
      ispublic: booleanNumber
      isfriend: booleanNumber
      isfamily: booleanNumber
    }[]
  }
}

type ContentProperty<T = string> = {
  _content: T
}

export interface FlickrGetInfoPeopleResponse {
  stat: string
  person: {
    id: string
    nsid: string
    ispro: number
    is_deleted: number
    iconserver: string
    iconfarm: number
    path_alias: null | any
    has_stats: number
    pro_badge: string
    expire: string
    gender: string
    ignored: number
    contact: number
    friend: number
    family: number
    revcontact: number
    revfriend: number
    revfamily: number
    username: ContentProperty
    realname: ContentProperty
    location: ContentProperty
    timezone: {
      label: string
      offset: string
      timezone_id: string
    }
    description: ContentProperty
    photosurl: ContentProperty
    profileurl: ContentProperty
    mobileurl: ContentProperty
    photos: {
      firstdatetaken: ContentProperty
      firstdate: ContentProperty
      count: ContentProperty<number>
    }
    has_adfree: number
    has_free_standard_shipping: number
    has_free_educational_resources: number
  }
}
