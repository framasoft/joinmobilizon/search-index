import { APMedia } from './activity-pub'
import { APLocation } from './address.model'

export interface Actor {
  id: string
  name: string
  displayName: string
  avatar: string
  description: string
  url: string
  host: string
  openness: 'invite_only' | 'moderated' | 'open'
  manuallyApprovesFollowers: boolean
  language: string
}

export interface APActor {
  id: string
  preferredUsername: string
  name: string
  icon: APMedia
  image: APMedia
  summary: string
  type: 'Group' | 'Person'
  openness: 'invite_only' | 'moderated' | 'open'
  manuallyApprovesFollowers: boolean
}
export interface APGroup extends APActor {
  type: 'Group'
  location?: APLocation
  memberCount?: number
}
export interface DBActor extends Actor {
  id: string
  name: string
  displayName: string
  description: string
  url: string
  host: string

  handle: string
  avatar: any
}
