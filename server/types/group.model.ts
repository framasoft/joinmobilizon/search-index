import { APGroup, DBActor } from './actor.model'
import { IndexableDoc } from './indexable-doc.model'
import { DBLocation, Location } from './address.model'

export interface Group {
  id: string
  name: string
  displayName: string
  avatar: string
  description: string
  url: string
  location?: Location
  host: string
  memberCount?: number
  openness: 'invite_only' | 'moderated' | 'open'
  manuallyApprovesFollowers: boolean
  language: string
}

export interface IndexableGroup extends APGroup, IndexableDoc {
  url: string
  handle: string
}

export interface DBGroup extends DBActor {
  indexedAt: Date
  handle: string
  url: string

  score?: number
  location?: DBLocation
  memberCount: number
}

// Results from the search API
export interface EnhancedGroup extends Group {
  score: number
}
