export interface MarkerAggregate {
  key: string
  doc_count: number
  gridCentroid: {
    location: {
      lat: number
      lon: number
    }
    count: number
  }
}
