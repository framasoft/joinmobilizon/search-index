import { APObject } from './activity-pub'
import { Actor, APActor, DBActor } from './actor.model'
import { Location, APLocation, DBLocation } from './address.model'
import { IndexableDoc } from './indexable-doc.model'

export enum EventStatus {
  TENTATIVE = 'TENTATIVE',
  CONFIRMED = 'CONFIRMED',
  CANCELLED = 'CANCELLED',
}

export interface APEvent extends APObject {
  actor: APActor
  anonymousParticipationEnabled: boolean
  attachment: { mediaType: string; name: string; type: string; url: string }[]
  attributedTo: APActor
  cc: string | (string | APActor)[]
  to: string | (string | APActor)[]
  commentsEnabled: boolean
  content: string
  draft: boolean
  endTime: string
  'ical:status': string
  joinMode: string
  maximumAttendeeCapacity: number
  mediaType: string
  name: string
  published: string
  repliesModerationOption: string
  startTime: string
  tag: any[]
  type: 'Event'
  updated: string
  url?: string
  uuid: string
  location?: APLocation
  inLanguage: string
  isOnline: boolean
  category: string
  remainingAttendeeCapacity: number
  participantCount?: number
}

export interface Event {
  id: string
  creator: Actor
  // anonymousParticipationEnabled: boolean
  group?: Actor | null
  // commentsEnabled: boolean
  // description: string
  endTime: string
  status: string
  joinMode: string
  maximumAttendeeCapacity: number
  name: string
  published: string
  location?: Location
  startTime: string
  tags: any[]
  updated: string
  url?: string
  uuid: string
  banner: string | null
  language?: string
  isOnline: boolean
  remainingAttendeeCapacity: number
  category: string
  participantCount: number
}

export interface IndexableEvent extends APEvent, IndexableDoc {}

export interface DBEvent extends Event {
  indexedAt: Date
  host: string
  url: string
  score?: number
  creator: DBActor
  group?: DBActor
  location?: DBLocation
  status: EventStatus
}

// Results from the search API
export interface EnhancedEvent extends Event {
  score: number
}
