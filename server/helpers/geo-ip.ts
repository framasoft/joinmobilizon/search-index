/**
 * Copied from https://github.com/Chocobozzz/PeerTube/blob/3329325a64c21067a598ea42a5c7eb0c489aedea/server/helpers/geo-ip.ts
 * AGPL
 */

import { pathExists, writeFile } from 'fs-extra'
import maxmind, { CityResponse, Reader } from 'maxmind'
import { join } from 'path'
import { CONFIG } from '../initializers/constants'
import { GeoIPResponse } from '../types/geo-ip'
import { logger } from './logger'
import { isBinaryResponse, searchEngineGot } from './requests'
import { Request } from 'express'

const mmbdFilename = 'dbip-city-lite-latest.mmdb'
const mmdbPath = join(CONFIG.STORAGE.GEO_IP_DIR, mmbdFilename)

export class GeoIP {
  private static instance: GeoIP

  private reader: Reader<CityResponse>

  private constructor() {}

  async safeCountryISOLookup(req: Request): Promise<GeoIPResponse | null> {
    if (CONFIG.GEO_IP.ENABLED === false) return null

    await this.initReaderIfNeeded()

    try {
      const { ip } = req
      const language = req.acceptsLanguages()[0].split('-')[0] || 'en'
      const result = this.reader.get(ip)
      if (!result) return null
      logger.debug({ result, language }, 'Found information for IP')

      const isoCode = result.country.iso_code
      return {
        country: {
          isoCode,
          name: result.country?.names[language] ?? result.country?.names.en,
        },
        centroid: [result.location.longitude, result.location.latitude],
        location: result.location,
      }
    } catch (err) {
      logger.error('Cannot get country from IP.', { err })

      return null
    }
  }

  async updateDatabase() {
    if (CONFIG.GEO_IP.ENABLED === false) return

    const url = CONFIG.GEO_IP.COUNTRY.DATABASE_URL

    logger.info('Updating GeoIP database from %s.', url)

    const gotOptions = { context: { bodyKBLimit: 200_000 }, responseType: 'buffer' as 'buffer' }

    try {
      const gotResult = await searchEngineGot(url, { ...gotOptions })

      if (!isBinaryResponse(gotResult)) {
        throw new Error('Not a binary response')
      }

      await writeFile(mmdbPath, gotResult.body)

      // Reinit reader
      this.reader = undefined

      logger.info('GeoIP database updated %s.', mmdbPath)
    } catch (err) {
      logger.error('Cannot update GeoIP database from %s.', url, { err })
    }
  }

  private async initReaderIfNeeded() {
    if (!this.reader) {
      if (!(await pathExists(mmdbPath))) {
        await this.updateDatabase()
      }

      this.reader = await maxmind.open(mmdbPath, { watchForUpdates: false })
    }
  }

  static get Instance() {
    return this.instance || (this.instance = new this())
  }
}
