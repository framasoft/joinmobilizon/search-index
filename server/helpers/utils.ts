import { Request, Response } from 'express'

function badRequest(req: Request, res: Response) {
  return res.type('json').status(400).end()
}

// ---------------------------------------------------------------------------

export { badRequest }
