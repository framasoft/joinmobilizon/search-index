import sanitizeHtml from 'sanitize-html'
import { convert } from 'html-to-text'

export const toSafeHtml = (text: string) => {
  if (!text) return ''

  // Convert to safe Html
  return sanitizeHtml(text, {
    allowedTags: ['a', 'span', 'br', 'strong', 'em', 'ul', 'ol', 'li'],
    allowedSchemes: ['http', 'https'],
    allowedAttributes: {
      a: ['href', 'class', 'target', 'rel'],
      '*': ['data-*'],
    },
    transformTags: {
      a: (tagName: string, attribs: any) => {
        let rel = 'noopener noreferrer'
        if (attribs.rel === 'me') rel += ' me'

        return {
          tagName,
          attribs: Object.assign(attribs, {
            target: '_blank',
            rel,
          }),
        }
      },
    },
  })
}

export const stripTags = (text: string) => {
  if (!text) return ''

  return sanitizeHtml(text, {
    allowedTags: [],
    allowedAttributes: {},
  })
}

export const htmlToText = (html: string): string => {
  if (!html) return ''
  return convert(html, { wordwrap: false })
}
