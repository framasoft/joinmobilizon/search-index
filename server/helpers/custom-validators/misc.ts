import validator from 'validator'
import { formatEventStatusForDB } from '../../lib/elastic-search/shared/schema-utils'

function exists(value: any) {
  return value !== undefined && value !== null
}

function isArray(value: any) {
  return Array.isArray(value)
}

function isDateValid(value: string) {
  return exists(value) && validator.isISO8601(value)
}

function areCoordinatesValid(value: string): boolean {
  const [lat, lon] = value.split(':')
  return validator.isNumeric(lat) && validator.isNumeric(lon)
}

function isDistanceValid(value: string): boolean {
  return value === 'anywhere' || value.slice(-3) === '_km'
}

function toArray(value: any) {
  if (value && isArray(value) === false) return [value]

  return value
}

// ---------------------------------------------------------------------------

export { areCoordinatesValid, exists, isArray, isDateValid, isDistanceValid, toArray }
