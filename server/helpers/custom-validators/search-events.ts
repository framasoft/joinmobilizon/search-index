import { isArray } from './misc'
import validator from 'validator'
import { formatEventStatusForDB } from '../../lib/elastic-search/shared/schema-utils'

function isNumberArray(value: any) {
  return isArray(value) && value.every((v) => validator.isInt('' + v))
}

function isStringArray(value: any) {
  return isArray(value) && value.every((v) => typeof v === 'string')
}

function isEventStatusArray(value: any): boolean {
  return isArray(value) && value.every((v) => formatEventStatusForDB(v) !== undefined)
}

// ---------------------------------------------------------------------------

export { isNumberArray, isStringArray, isEventStatusArray }
