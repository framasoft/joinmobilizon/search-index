import { getWebserverUrl } from '../initializers/constants'
import got, { CancelableRequest, NormalizedOptions, Options as GotOptions, RequestError, Response } from 'got'
import {
  ACTIVITY_PUB,
  BINARY_CONTENT_TYPES,
  SEARCH_ENGINE_VERSION,
  REQUEST_TIMEOUTS,
  REQUESTS,
} from '../initializers/constants'
import { logger } from './logger'

type MobilizonSearchEngineRequestOptions = {
  timeout?: number
  activityPub?: boolean
  bodyKBLimit?: number // 1MB
  httpSignature?: {
    algorithm: string
    authorizationHeaderName: string
    keyId: string
    key: string
    headers: string[]
  }
  retries?: number
  jsonResponse?: boolean
} & Pick<GotOptions, 'headers' | 'json' | 'method' | 'searchParams'>

export interface MobilizonSearchEngineRequestError extends Error {
  statusCode?: number
  responseBody?: any
  responseHeaders?: any
}

// function doRequest<T>(requestOptions: CoreOptions & UriOptions): Bluebird<{ response: RequestResponse; body: T }> {
//   if (!requestOptions.headers) requestOptions.headers = {}

//   requestOptions.headers['User-Agent'] = `Mobilizon search index (+${getWebserverUrl()})`

//   return new Bluebird<{ response: RequestResponse; body: T }>((res, rej) => {
//     request(requestOptions, (err, response, body) => (err ? rej(err) : res({ response, body })))
//   })
// }

// async function doRequestWithRetries<T>(
//   url: string,
//   requestOptions: MobilizonSearchEngineRequestOptions,
//   maxRetries: number,
//   msToWait: number,
//   currentRetry = 0
// ): Promise<{ response: Response<any>; body: T }> {
//   const updatedRequestOptions = Object.assign({}, requestOptions, { timeout: 20000 })

//   const res = await doRequest<T>(url, updatedRequestOptions)

//   if (res.statusCode === 429) {
//     if (currentRetry < maxRetries) {
//       await waitMs(msToWait)
//       return doRequestWithRetries(url, requestOptions, maxRetries, msToWait, currentRetry + 1)
//     }

//     throw new Error('Exceeded max retries for request ' + url)
//   }

//   return res
// }

const searchEngineGot = got.extend({
  headers: {
    'user-agent': getUserAgent(),
  },

  handlers: [
    (options, next) => {
      const promiseOrStream = next(options) as CancelableRequest<any>
      const bodyKBLimit = options.context?.bodyKBLimit as number
      if (!bodyKBLimit) throw new Error('No KB limit for this request')

      const bodyLimit = bodyKBLimit * 1000

      /* eslint-disable @typescript-eslint/no-floating-promises */
      promiseOrStream.on('downloadProgress', (progress) => {
        if (progress.transferred > bodyLimit && progress.percent !== 1) {
          const message = `Exceeded the download limit of ${bodyLimit} B`
          logger.warn(message)

          // CancelableRequest
          if (promiseOrStream.cancel) {
            promiseOrStream.cancel()
            return
          }

          // Stream
          ;(promiseOrStream as any).destroy()
        }
      })

      return promiseOrStream
    },
  ],

  hooks: {
    beforeRequest: [
      (options) => {
        const headers = options.headers || {}
        headers['host'] = options.url.host
      },
    ],

    beforeRetry: [
      (_options: NormalizedOptions, error: RequestError, retryCount: number) => {
        logger.debug('Retrying request to %s.', error.request.requestUrl, {
          retryCount,
          error: buildRequestError(error),
        })
      },
    ],
  },
})

function doRequest<T>(url: string, options: MobilizonSearchEngineRequestOptions = {}) {
  const gotOptions = buildGotOptions(options)

  return searchEngineGot<T>(url, gotOptions).catch((err) => {
    throw buildRequestError(err)
  })
}

function doJSONRequest<T>(url: string, options: MobilizonSearchEngineRequestOptions = {}) {
  const gotOptions = buildGotOptions(options)

  return searchEngineGot<T>(url, { ...gotOptions, responseType: 'json' }).catch((err) => {
    throw buildRequestError(err)
  })
}

function isBinaryResponse(result: Response<any>) {
  return BINARY_CONTENT_TYPES.has(result.headers['content-type'])
}

export { doRequest, doJSONRequest, searchEngineGot, isBinaryResponse }

function buildGotOptions(options: MobilizonSearchEngineRequestOptions) {
  const { activityPub, bodyKBLimit = 1000 } = options

  const context = { bodyKBLimit, httpSignature: options.httpSignature }

  let headers = options.headers || {}

  if (!headers.date) {
    headers = { ...headers, date: new Date().toUTCString() }
  }

  if (activityPub && !headers.accept) {
    headers = { ...headers, accept: ACTIVITY_PUB.ACCEPT_HEADER }
  }

  return {
    method: options.method,
    dnsCache: true,
    timeout: options.timeout ?? REQUEST_TIMEOUTS.DEFAULT,
    json: options.json,
    searchParams: options.searchParams,
    retry: options.retries ?? REQUESTS.MAX_RETRIES,
    headers,
    context,
  }
}

function getUserAgent() {
  return `Mobilizon Search Engine/${SEARCH_ENGINE_VERSION} (+${getWebserverUrl()})`
}

function buildRequestError(error: RequestError) {
  const newError: MobilizonSearchEngineRequestError = new Error(error.message)
  newError.name = error.name
  newError.stack = error.stack

  if (error.response) {
    newError.responseBody = error.response.body
    newError.responseHeaders = error.response.headers
    newError.statusCode = error.response.statusCode
  }

  return newError
}
