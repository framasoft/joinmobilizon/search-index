# Application behind search.joinmobilizon.org

## Requirements

Pre-requirements:
* NodeJS
* An ElasticSearch server (7.10+)

Optional dependencies:
* An unsplash or Flickr API key to display pictures from cities where users are geolocated
* A reverse-geocoding server to get the city/region where users are from their coordinates (OpenStreetMap's Nominatim server is used by default)
* An [ImgProxy](https://imgproxy.net) server to load the remote pictures from events and groups locally

## Dev

```terminal
$ yarn install --pure-lockfile
$ cp config/default.yaml config/local-development.yaml
```

Edit the `config/local-development.yaml` file to match your ElasticSearch credentials, personalize the server name, header picture and fill-in details from optional dependencies.

The database (Elastic Search) is automatically created by the search index app at startup.

Run simultaneously (for example with 3 terminals):

```terminal
$ npx tsc -w
```

```terminal
$ node dist/server
```

```
$ cd client && yarn run vue-gettext-compile && npm run dev
```

Then open http://localhost:5173.

### Add locale

Add the locale in `client/src/main.ts` and `client/Makefile`. Then update translations.

## Production

Install dependencies:
  * NodeJS
  * Elastic Search

```terminal
$ git clone https://framagit.org/framasoft/joinmobilizon/search-index.git /var/www/mobilizon-search-index
$ cd /var/www/mobilizon-search-index
$ yarn install --pure-lockfile
$ cd client && yarn run vue-gettext-compile && cd ../
$ npm run build
$ cp config/default.yaml config/production.yaml
$ vim config/production.yaml
$ NODE_ENV=production NODE_CONFIG_DIR=/var/www/mobilizon-search-index/config node dist/server.js
```

You can use the nginx configuration template and the systemd service template in `support/` to help you setup the service. Adapt the values for ports and remove the sections you don't use.

### Mapping migration

To update Elastic Search index mappings without downtime, run another instance of the search indexer
using the same configuration that the main node. You just have to update `elastic-search.indexes.*` to use new index names.

```
$ cd /var/www/mobilizon-search-index
$ cp config/production.yaml config/production-1.yaml
$ vim config/production-1.yaml
$ NODE_ENV=production NODE_APP_INSTANCE=1 NODE_CONFIG_DIR=/var/www/mobilizon-search-index/config node dist/server.js
```

After a while the new indexes will be filled. You can then stop the second indexer, update `config/production.yaml` to use
the new index names and restart the main index.

