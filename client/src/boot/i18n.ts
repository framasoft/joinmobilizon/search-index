// ############# I18N ##############

import { createGettext } from 'vue3-gettext'
import defaultDateFnsLocale from 'date-fns/esm/locale/en-US'
import en_US from '../locale/en_US.json'

const availableLanguages = {
  en_US: 'English',
  fr_FR: 'Français',
  de: 'Deutsch',
  el: 'ελληνικά',
  es: 'Español',
  gd: 'Gàidhlig',
  gl: 'galego',
  ru: 'русский',
  it: 'Italiano',
  ja: '日本語',
  sv: 'svenska',
  nl: 'Nederlands',
  oc: 'Occitan',
  sq: 'Shqip',
  pt_BR: 'Português (Brasil)',
  bn: 'বাংলা',
  pl: 'Polski',
}
const aliasesLanguages = {
  en: 'en_US',
  fr: 'fr_FR',
  br: 'pt_BR',
  pt: 'pt_BR',
}
const allLocales = Object.keys(availableLanguages).concat(
  Object.keys(aliasesLanguages)
)

const defaultLanguage = 'en_US'
let currentLanguage = defaultLanguage

const basePath = import.meta.env.BASE_URL
const startRegexp = new RegExp('^' + basePath)

const paths = window.location.pathname.replace(startRegexp, '').split('/')

const localePath = paths.length !== 0 ? paths[0] : ''
const languageFromLocalStorage = localStorage.getItem('language')

if (allLocales.includes(localePath)) {
  currentLanguage = aliasesLanguages[localePath]
    ? aliasesLanguages[localePath]
    : localePath
  localStorage.setItem('language', currentLanguage)
} else if (languageFromLocalStorage) {
  currentLanguage = languageFromLocalStorage
} else {
  const navigatorLanguage =
    (window.navigator as any).userLanguage || window.navigator.language
  const snakeCaseLanguage = navigatorLanguage.replace('-', '_')
  currentLanguage = aliasesLanguages[snakeCaseLanguage]
    ? aliasesLanguages[snakeCaseLanguage]
    : snakeCaseLanguage
}

if (!allLocales.includes(currentLanguage)) {
  currentLanguage = defaultLanguage
}

async function loadTranslation(locale) {
  if (!allLocales.includes(locale)) {
    locale = defaultLanguage
  }
  return (await import(`../locale/${locale}.json`)).default
}

async function loadNewTranslation(gettext, locale) {
  gettext.translations = {
    ...gettext.translations,
    ...(await loadTranslation(locale)),
  }
  gettext.current = locale
}

async function generateGettext() {
  return createGettext({
    translations: {
      ...en_US,
      ...(await loadTranslation(currentLanguage)),
    },
    availableLanguages,
    defaultLanguage: currentLanguage,
    mutedLanguages: ['en_US'],
  })
}

export {
  generateGettext,
  loadNewTranslation,
  currentLanguage,
  allLocales,
  defaultDateFnsLocale,
}
