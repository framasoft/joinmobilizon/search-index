msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2023-11-24 04:22+0000\n"
"Last-Translator: Fábio Tramasoli <fabio@tramasoli.com>\n"
"Language-Team: Portuguese (Brazil) <https://weblate.framasoft.org/projects/"
"mobilizon/search-index/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.2\n"
"Generated-By: easygettext\n"

#: src/views/Home.vue:239
msgid "%{indexName} - A search engine for Mobilizon events and groups"
msgstr ""

#: src/components/SearchWarning.vue:13 src/components/SearchWarning.vue:17
#: src/components/SearchWarning.vue:19 src/components/SearchWarning.vue:5
#: src/components/SearchWarning.vue:6
msgid ""
"%{indexName} displays events and groups that match your search but is not "
"the publisher, nor the owner. If you notice any problems with an event, "
"report it to the administrators on the Mobilizon website where the event is "
"published."
msgstr ""

#: src/views/Search.vue:103 src/views/Search.vue:104 src/views/Search.vue:106
#: src/views/Search.vue:108 src/views/Search.vue:123 src/views/Search.vue:266
#: src/views/Search.vue:284
msgid "%{numberOfCategories} selected"
msgstr ""

#: src/views/Search.vue:162 src/views/Search.vue:163 src/views/Search.vue:165
#: src/views/Search.vue:167 src/views/Search.vue:182 src/views/Search.vue:376
#: src/views/Search.vue:393
msgid "%{numberOfLanguages} selected"
msgstr ""

#: src/views/Search.vue:1284
msgid "%{search} - %{indexName}"
msgstr ""

#: src/components/Header.vue:10 src/components/Header.vue:12
#: src/components/Header.vue:13 src/components/Header.vue:9
msgid "A search engine of %{linkStart}Mobilizon%{linkEnd} events and groups"
msgstr ""

#: src/views/Home.vue:46 src/views/Home.vue:47 src/views/Home.vue:62
#: src/views/Home.vue:65 src/views/Home.vue:67
msgid "Allow us to detect your position to find nearby events and groups"
msgstr ""

#: src/query/constants.ts:447
msgid "Any"
msgstr "Qualquer"

#: src/query/constants.ts:501
msgid "Any distance"
msgstr ""

#: src/views/Search.vue:257 src/views/Search.vue:258 src/views/Search.vue:260
#: src/views/Search.vue:262 src/views/Search.vue:277 src/views/Search.vue:502
msgid "Apply filters"
msgstr "Aplicar filtros"

#: src/query/constants.ts:117
msgid "Arts"
msgstr ""

#: src/query/constants.ts:155
msgid "Auto, Boat & Air"
msgstr ""

#: src/query/constants.ts:586
msgid "Best match"
msgstr "Mais relevante"

#: src/query/constants.ts:122
msgid "Book Clubs"
msgstr ""

#: src/query/constants.ts:127
msgid "Business"
msgstr ""

#: src/components/SetupInstanceSuggestion.vue:107
#: src/components/SetupInstanceSuggestion.vue:46
#: src/components/SetupInstanceSuggestion.vue:77
#: src/components/SetupInstanceSuggestion.vue:86
#: src/components/SetupInstanceSuggestion.vue:90
msgid "Cancel"
msgstr ""

#: src/query/constants.ts:563
msgid "Cancelled"
msgstr ""

#: src/query/constants.ts:24
msgid "Català"
msgstr "Catalã"

#: src/views/Search.vue:113 src/views/Search.vue:256 src/views/Search.vue:260
#: src/views/Search.vue:93 src/views/Search.vue:94 src/views/Search.vue:96
#: src/views/Search.vue:98
#, fuzzy
msgid "Categories"
msgstr "Categoria"

#: src/views/Categories.vue:28 src/views/Categories.vue:29
#: src/views/Categories.vue:38 src/views/Categories.vue:40
msgid "Category illustrations credits"
msgstr ""

#: src/query/constants.ts:131
msgid "Causes"
msgstr ""

#: src/query/constants.ts:28
msgid "Čeština"
msgstr ""

#: src/components/InterfaceLanguageDropdown.vue:12
#: src/components/InterfaceLanguageDropdown.vue:3
#: src/components/InterfaceLanguageDropdown.vue:4
#: src/components/InterfaceLanguageDropdown.vue:6
msgid "Change interface language"
msgstr "Mudar a língua da interface"

#: src/views/Search.vue:449 src/views/Search.vue:450 src/views/Search.vue:712
#: src/views/Search.vue:718 src/views/Search.vue:807 src/views/Search.vue:822
#: src/views/Search.vue:836
msgid "Change the filters."
msgstr ""

#: src/components/Footer.vue:58 src/components/Footer.vue:59
#: src/components/Footer.vue:61 src/components/Footer.vue:67
#: src/components/Footer.vue:69 src/components/Footer.vue:73
msgid "Chat"
msgstr ""

#: src/views/Search.vue:251 src/views/Search.vue:252 src/views/Search.vue:254
#: src/views/Search.vue:256 src/views/Search.vue:271 src/views/Search.vue:480
#: src/views/Search.vue:491 src/views/Search.vue:493
msgid "Clear"
msgstr ""

#: src/components/SearchWarning.vue:16 src/components/SearchWarning.vue:17
#: src/components/SearchWarning.vue:24 src/components/SearchWarning.vue:34
#: src/components/SearchWarning.vue:38
#: src/components/SetupInstanceSuggestion.vue:16
#: src/components/SetupInstanceSuggestion.vue:17
#: src/components/SetupInstanceSuggestion.vue:24
#: src/components/SetupInstanceSuggestion.vue:34
#: src/components/SetupInstanceSuggestion.vue:38
msgid "Close"
msgstr ""

#: src/query/constants.ts:135
msgid "Comedy"
msgstr "Comédia"

#: src/components/Footer.vue:51 src/components/Footer.vue:52
#: src/components/Footer.vue:54 src/components/Footer.vue:60
#: src/components/Footer.vue:62 src/query/constants.ts:159
msgid "Community"
msgstr ""

#: src/query/constants.ts:555
msgid "Confirmed"
msgstr ""

#: src/query/constants.ts:139
msgid "Crafts"
msgstr ""

#: src/components/Footer.vue:15 src/components/Footer.vue:16
#: src/components/Footer.vue:18 src/components/Footer.vue:20
#, fuzzy
msgid "Create groups"
msgstr "Criar playlists"

#: src/components/result/PostResult.vue:18
#: src/components/result/PostResult.vue:19
#: src/components/result/PostResult.vue:25
#: src/components/result/PostResult.vue:31
msgid "Created by"
msgstr "Criado por"

#: src/query/constants.ts:40
msgid "Deutsch"
msgstr "Alemão"

#: src/components/Header.vue:21
msgid "Developed by %{framasoft}"
msgstr ""

#: src/components/Footer.vue:45 src/components/Footer.vue:46
#: src/components/Footer.vue:48 src/components/Footer.vue:54
#: src/components/Footer.vue:56
msgid "Discover Mobilizon"
msgstr ""

#: src/components/result/GroupResult.vue:1
#: src/components/result/GroupResult.vue:6
#, fuzzy
msgid "Discover this group on %{host}"
msgstr "Descubra esse canal em %{host}"

#: src/views/Search.vue:219 src/views/Search.vue:223 src/views/Search.vue:76
#: src/views/Search.vue:77 src/views/Search.vue:79 src/views/Search.vue:81
#: src/views/Search.vue:96
msgid "Distance"
msgstr ""

#: src/query/constants.ts:8
msgid "English"
msgstr "Inglês"

#: src/query/constants.ts:52
msgid "Español"
msgstr "Espanhol"

#: src/query/constants.ts:32
msgid "Esperanto"
msgstr "Esperanto"

#: src/query/constants.ts:20
msgid "Euskara"
msgstr ""

#: src/query/constants.ts:594 src/views/Search.vue:180 src/views/Search.vue:184
#: src/views/Search.vue:57 src/views/Search.vue:58 src/views/Search.vue:60
#: src/views/Search.vue:62 src/views/Search.vue:77
msgid "Event date"
msgstr ""

#: src/views/Search.vue:126 src/views/Search.vue:127 src/views/Search.vue:129
#: src/views/Search.vue:131 src/views/Search.vue:146 src/views/Search.vue:316
#: src/views/Search.vue:320
msgid "Event status"
msgstr ""

#: src/query/constants.ts:104
msgid "Events"
msgstr ""

#: src/views/Search.vue:1294
msgid "Events for category %{category} - %{indexName}"
msgid_plural "Events for categories %{category} - %{indexName}"
msgstr[0] ""
msgstr[1] ""

#: src/components/local/CloseEvents.vue:4
#: src/components/local/CloseEvents.vue:5
#, fuzzy
msgid "Events nearby %{position}"
msgstr "Assista o vídeo em %{host}"

#: src/query/constants.ts:100
msgid "Everything"
msgstr ""

#: src/views/Home.vue:42 src/views/Home.vue:43 src/views/Home.vue:58
#: src/views/Home.vue:61
msgid "Explore events and groups near you"
msgstr ""

#: src/components/search/SearchFields.vue:90
msgid "Explore!"
msgstr ""

#: src/query/constants.ts:163
msgid "Family & Education"
msgstr ""

#: src/query/constants.ts:167
msgid "Fashion & Beauty"
msgstr ""

#: src/query/constants.ts:171
msgid "Film & Media"
msgstr ""

#: src/views/Search.vue:1308
msgid "Find events and groups - %{indexName}"
msgstr ""

#: src/query/constants.ts:143
msgid "Food & Drink"
msgstr ""

#: src/query/constants.ts:12
msgid "Français"
msgstr "Francês"

#: src/query/constants.ts:60
msgid "Gàidhlig"
msgstr ""

#: src/query/constants.ts:175
msgid "Games"
msgstr ""

#: src/components/local/CloseContent.vue:10
#: src/components/local/CloseContent.vue:7
#: src/components/local/CloseContent.vue:8 src/views/Home.vue:59
#: src/views/Home.vue:60 src/views/Home.vue:75 src/views/Home.vue:80
#: src/views/Home.vue:98
msgid "Geolocate me"
msgstr ""

#: src/components/Footer.vue:11 src/components/Footer.vue:6
#: src/components/Footer.vue:7 src/components/Footer.vue:9
msgid "Getting started"
msgstr ""

#: src/components/ActorMiniature.vue:11 src/components/ActorMiniature.vue:4
#: src/components/ActorMiniature.vue:5
#, fuzzy
msgid "Go on this group page"
msgstr "Ir para a página desta conta"

#: src/components/search/SearchFields.vue:89
msgid "Go!"
msgstr ""

#: src/query/constants.ts:108
msgid "Groups"
msgstr ""

#: src/query/constants.ts:147
msgid "Health"
msgstr ""

#: src/views/Search.vue:11 src/views/Search.vue:12 src/views/Search.vue:14
#: src/views/Search.vue:16 src/views/Search.vue:265 src/views/Search.vue:266
#: src/views/Search.vue:268 src/views/Search.vue:270 src/views/Search.vue:285
#: src/views/Search.vue:31 src/views/Search.vue:513 src/views/Search.vue:528
#, fuzzy
msgid "Hide filters"
msgstr "Aplicar filtros"

#: src/components/Footer.vue:20 src/components/Footer.vue:21
#: src/components/Footer.vue:23 src/components/Footer.vue:25
msgid "Host it yourself"
msgstr ""

#: src/components/SetupInstanceSuggestion.vue:13
#: src/components/SetupInstanceSuggestion.vue:17
#: src/components/SetupInstanceSuggestion.vue:19
#: src/components/SetupInstanceSuggestion.vue:5
#: src/components/SetupInstanceSuggestion.vue:6
msgid ""
"If you already have a Mobilizon account somewhere, %{indexName} can redirect "
"you to your own instance to interact with events and groups."
msgstr ""

#: src/views/Categories.vue:44 src/views/Categories.vue:45
#: src/views/Categories.vue:54 src/views/Categories.vue:56
#: src/views/Categories.vue:71 src/views/Categories.vue:92
msgid ""
"Illustration picture for “%{category}” by %{author} on %{source} (%{license})"
msgstr ""

#: src/components/result/PostResult.vue:23
#: src/components/result/PostResult.vue:24
#: src/components/result/PostResult.vue:30
#: src/components/result/PostResult.vue:36
#: src/components/result/PostResult.vue:39
msgid "In"
msgstr "Em"

#: src/query/constants.ts:455
msgid "In the future"
msgstr ""

#: src/query/constants.ts:451
msgid "In the past"
msgstr ""

#: src/components/Footer.vue:24 src/components/Footer.vue:25
#: src/components/Footer.vue:27 src/components/Footer.vue:29
msgid "Install Mobilizon"
msgstr ""

#: src/query/constants.ts:44
msgid "Italiano"
msgstr "Italiano"

#: src/components/search/SearchFields.vue:42
msgid "Keyword, event title, group name, etc."
msgstr ""

#: src/query/constants.ts:179
msgid "Language and Culture"
msgstr ""

#: src/views/Search.vue:152 src/views/Search.vue:153 src/views/Search.vue:155
#: src/views/Search.vue:157 src/views/Search.vue:172 src/views/Search.vue:365
#: src/views/Search.vue:366 src/views/Search.vue:369
#, fuzzy
msgid "Languages"
msgstr "Idioma"

#: src/query/constants.ts:183
msgid "Learning"
msgstr ""

#: src/query/constants.ts:602
#, fuzzy
msgid "Least recently published"
msgstr "Mais recente"

#: src/query/constants.ts:187
msgid "LGBTQ"
msgstr ""

#: src/views/Search.vue:376 src/views/Search.vue:377 src/views/Search.vue:639
#: src/views/Search.vue:645 src/views/Search.vue:648 src/views/Search.vue:675
#: src/views/Search.vue:679 src/views/Search.vue:697
msgid "List"
msgstr ""

#: src/components/Loading.vue:6 src/components/Loading.vue:7
msgid "Loading"
msgstr ""

#: src/views/Search.vue:459 src/views/Search.vue:460 src/views/Search.vue:722
#: src/views/Search.vue:728 src/views/Search.vue:852 src/views/Search.vue:855
msgid "Loading map"
msgstr ""

#: src/components/search/LocationAutocomplete.vue:8
#: src/components/search/SearchFields.vue:21
#: src/components/search/SearchFields.vue:22
#: src/components/search/SearchFields.vue:65
#, fuzzy
msgid "Location"
msgstr "Educação"

#: src/views/Search.vue:441 src/views/Search.vue:442 src/views/Search.vue:704
#: src/views/Search.vue:710 src/views/Search.vue:799 src/views/Search.vue:814
#: src/views/Search.vue:828
msgid "Make sure that all words are spelled correctly."
msgstr ""

#: src/views/Search.vue:364 src/views/Search.vue:365 src/views/Search.vue:627
#: src/views/Search.vue:633 src/views/Search.vue:636 src/views/Search.vue:663
#: src/views/Search.vue:667 src/views/Search.vue:670
msgid "Map"
msgstr ""

#: src/query/constants.ts:598
#, fuzzy
msgid "Most recently published"
msgstr "Mais recente"

#: src/query/constants.ts:191
msgid "Movements and politics"
msgstr ""

#: src/query/constants.ts:151
msgid "Music"
msgstr "Música"

#: src/query/constants.ts:48
msgid "Nederlands"
msgstr "Países Baixos - Holanda"

#: src/query/constants.ts:195
msgid "Networking"
msgstr ""

#: src/query/constants.ts:483
msgid "Next month"
msgstr ""

#: src/components/Pagination.vue:23 src/components/Pagination.vue:24
#: src/components/Pagination.vue:37
msgid "Next page"
msgstr "Próxima página"

#: src/query/constants.ts:475
#, fuzzy
msgid "Next week"
msgstr "Próxima página"

#: src/views/Search.vue:280 src/views/Search.vue:281 src/views/Search.vue:543
#: src/views/Search.vue:549 src/views/Search.vue:552
#, fuzzy
msgid "No events found"
msgstr "Nenhum resultado encontrado para"

#: src/views/Search.vue:283 src/views/Search.vue:284 src/views/Search.vue:546
#: src/views/Search.vue:552 src/views/Search.vue:555
#, fuzzy
msgid "No groups found"
msgstr "Nenhum resultado encontrado para"

#: src/views/Search.vue:285 src/views/Search.vue:286 src/views/Search.vue:548
#: src/views/Search.vue:554 src/views/Search.vue:557
#, fuzzy
msgid "No results found"
msgstr "Nenhum resultado encontrado para"

#: src/query/constants.ts:614
msgid "Number of members"
msgstr ""

#: src/query/constants.ts:56
msgid "Occitan"
msgstr ""

#: src/components/Category.vue:16 src/components/Category.vue:23
#: src/components/Category.vue:47
#, fuzzy
msgid "One event"
msgid_plural "%{count} events"
msgstr[0] "Nenhum resultado encontrado para"
msgstr[1] "Nenhum resultado encontrado para"

#: src/views/Search.vue:290 src/views/Search.vue:291 src/views/Search.vue:553
#: src/views/Search.vue:559 src/views/Search.vue:562
#, fuzzy
msgid "One event found"
msgid_plural "%{eventsCount} events found"
msgstr[0] "Nenhum resultado encontrado para"
msgstr[1] "Nenhum resultado encontrado para"

#: src/views/Search.vue:300 src/views/Search.vue:301 src/views/Search.vue:563
#: src/views/Search.vue:569 src/views/Search.vue:572
#, fuzzy
msgid "One group found"
msgid_plural "%{groupsCount} groups found"
msgstr[0] "Nenhum resultado encontrado para"
msgstr[1] "Nenhum resultado encontrado para"

#: src/query/constants.ts:505 src/query/constants.ts:511
#: src/query/constants.ts:517 src/query/constants.ts:523
#: src/query/constants.ts:529 src/query/constants.ts:535
msgid "one kilometer"
msgid_plural "%{number} kilometers"
msgstr[0] ""
msgstr[1] ""

#: src/components/result/GroupResult.vue:106
#: src/components/result/GroupResult.vue:110
#: src/components/result/GroupResult.vue:51
#, fuzzy
msgid "One member"
msgid_plural "%{memberCount} members"
msgstr[0] "Nenhum resultado encontrado para"
msgstr[1] "Nenhum resultado encontrado para"

#: src/components/result/EventResult.vue:136
#: src/components/result/EventResult.vue:99
msgid "One participant"
msgid_plural "%{participantCount} participants"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:310 src/views/Search.vue:311 src/views/Search.vue:573
#: src/views/Search.vue:579 src/views/Search.vue:582
#, fuzzy
msgid "One result found"
msgid_plural "%{resultsCount} result found"
msgstr[0] "Nenhum resultado encontrado para"
msgstr[1] "Nenhum resultado encontrado para"

#: src/views/Search.vue:157 src/views/Search.vue:160 src/views/Search.vue:170
#: src/views/Search.vue:50 src/views/Search.vue:51 src/views/Search.vue:53
#: src/views/Search.vue:55 src/views/Search.vue:70
msgid "Online events"
msgstr ""

#: src/components/local/OnlineEvents.vue:3
#: src/components/local/OnlineEvents.vue:4
#: src/components/local/OnlineEvents.vue:7
msgid "Online upcoming events"
msgstr ""

#: src/components/Footer.vue:11 src/components/Footer.vue:12
#: src/components/Footer.vue:14 src/components/Footer.vue:16
msgid "Open an account"
msgstr ""

#: src/query/constants.ts:235
msgid "Other"
msgstr ""

#: src/query/constants.ts:215
msgid "Outdoors and adventure"
msgstr ""

#: src/query/constants.ts:199
msgid "Party"
msgstr ""

#: src/query/constants.ts:203
msgid "Performing & visual arts"
msgstr ""

#: src/query/constants.ts:207
msgid "Pets"
msgstr ""

#: src/components/local/MoreContent.vue:12
#: src/components/local/MoreContent.vue:13
#: src/components/local/MoreContent.vue:16
#: src/components/local/MoreContent.vue:22
#: src/components/local/MoreContent.vue:24
#: src/components/local/MoreContent.vue:26
#: src/components/local/MoreContent.vue:40
msgid "Photo by %{author} on %{source}"
msgstr ""

#: src/query/constants.ts:211
msgid "Photography"
msgstr ""

#: src/query/constants.ts:76
msgid "Polski"
msgstr ""

#: src/components/local/CloseGroups.vue:4
#: src/components/local/CloseGroups.vue:5
msgid "Popular groups nearby %{position}"
msgstr ""

#: src/query/constants.ts:68
msgid "Português (Portugal)"
msgstr "Português (Portugal)"

#: src/components/Pagination.vue:5 src/components/Pagination.vue:6
msgid "Previous page"
msgstr "Página anterior"

#: src/components/result/PostResult.vue:95
#, fuzzy
msgid "Read the post on %{host}"
msgstr "Assista o vídeo em %{host}"

#: src/views/Search.vue:239 src/views/Search.vue:240 src/views/Search.vue:242
#: src/views/Search.vue:244 src/views/Search.vue:259 src/views/Search.vue:479
#: src/views/Search.vue:480
msgid "Redirection activated to the instance %{instance} (%{host})"
msgstr ""

#: src/components/Footer.vue:40 src/components/Footer.vue:41
#: src/components/Footer.vue:43 src/components/Footer.vue:49
#: src/components/Footer.vue:51
msgid "Resources"
msgstr ""

#: src/components/SetupInstanceSuggestion.vue:113
#: src/components/SetupInstanceSuggestion.vue:49
#: src/components/SetupInstanceSuggestion.vue:80
#: src/components/SetupInstanceSuggestion.vue:89
#: src/components/SetupInstanceSuggestion.vue:93
msgid "Save"
msgstr ""

#: src/query/constants.ts:223
#, fuzzy
msgid "Science and tech"
msgstr "Ciência e Tecnologia"

#: src/views/Categories.vue:5 src/views/Categories.vue:6
#: src/views/Categories.vue:9 src/views/Home.vue:11 src/views/Home.vue:12
#: src/views/Home.vue:19
msgid ""
"Search for your favorite events and groups on %{tagStart}one Mobilizon "
"website%{tagEnd} indexed by %{indexName}!"
msgid_plural ""
"Search for your favorite events and groups on %{tagStart}%{instancesCount} "
"Mobilizon websites%{tagEnd} indexed by %{indexName}!"
msgstr[0] ""
msgstr[1] ""

#: src/views/Home.vue:248
msgid ""
"Search for your favorite events and groups on one Mobilizon website indexed "
"by %{indexName}!"
msgid_plural ""
"Search for your favorite events and groups on %{instancesCount} Mobilizon "
"websites indexed by %{indexName}!"
msgstr[0] ""
msgstr[1] ""

#: src/views/Search.vue:1316
msgid "Search results on %{indexName}"
msgstr ""

#: src/components/SetupInstanceSuggestion.vue:12
#: src/components/SetupInstanceSuggestion.vue:13
#: src/components/SetupInstanceSuggestion.vue:18
#: src/components/SetupInstanceSuggestion.vue:20
#: src/components/SetupInstanceSuggestion.vue:24
#: src/components/SetupInstanceSuggestion.vue:27
#: src/components/SetupInstanceSuggestion.vue:29
#: src/components/SetupInstanceSuggestion.vue:60
#: src/components/SetupInstanceSuggestion.vue:69
#: src/components/SetupInstanceSuggestion.vue:73
#: src/components/SetupInstanceSuggestion.vue:75
msgid "Set my instance"
msgstr ""

#: src/views/Search.vue:12 src/views/Search.vue:13 src/views/Search.vue:15
#: src/views/Search.vue:17 src/views/Search.vue:32
#, fuzzy
msgid "Show filters"
msgstr "Aplicar filtros"

#: src/views/Search.vue:326 src/views/Search.vue:327 src/views/Search.vue:589
#: src/views/Search.vue:595 src/views/Search.vue:598 src/views/Search.vue:600
#: src/views/Search.vue:602
#, fuzzy
msgid "Sort"
msgstr "Ordenar por:"

#: src/components/Footer.vue:55 src/components/Footer.vue:56
#: src/components/Footer.vue:58 src/components/Footer.vue:64
#: src/components/Footer.vue:66
msgid "Source code"
msgstr "Código fonte"

#: src/query/constants.ts:219
msgid "Spirituality, religion and beliefs"
msgstr ""

#: src/query/constants.ts:227
msgid "Sports"
msgstr "Esportes"

#: src/views/Search.vue:436 src/views/Search.vue:437 src/views/Search.vue:699
#: src/views/Search.vue:705 src/views/Search.vue:794 src/views/Search.vue:809
#: src/views/Search.vue:823
msgid "Suggestions:"
msgstr ""

#: src/query/constants.ts:80
msgid "suomi"
msgstr ""

#: src/query/constants.ts:72
msgid "svenska"
msgstr ""

#: src/query/constants.ts:559
msgid "Tentative"
msgstr ""

#: src/query/constants.ts:231
msgid "Theatre"
msgstr ""

#: src/query/constants.ts:479
msgid "This month"
msgstr ""

#: src/query/constants.ts:471
msgid "This week"
msgstr ""

#: src/query/constants.ts:467
msgid "This weekend"
msgstr ""

#: src/query/constants.ts:459
msgid "Today"
msgstr "Hoje"

#: src/query/constants.ts:463
msgid "Tomorrow"
msgstr ""

#: src/views/Search.vue:446 src/views/Search.vue:447 src/views/Search.vue:709
#: src/views/Search.vue:715 src/views/Search.vue:804 src/views/Search.vue:819
#: src/views/Search.vue:833
msgid "Try different keywords."
msgstr ""

#: src/views/Search.vue:448 src/views/Search.vue:449 src/views/Search.vue:711
#: src/views/Search.vue:717 src/views/Search.vue:806 src/views/Search.vue:821
#: src/views/Search.vue:835
msgid "Try fewer keywords."
msgstr ""

#: src/views/Search.vue:447 src/views/Search.vue:448 src/views/Search.vue:710
#: src/views/Search.vue:716 src/views/Search.vue:805 src/views/Search.vue:820
#: src/views/Search.vue:834
msgid "Try more general keywords."
msgstr ""

#: src/views/Search.vue:15 src/views/Search.vue:16 src/views/Search.vue:18
#: src/views/Search.vue:20 src/views/Search.vue:35 src/views/Search.vue:39
msgid "Type"
msgstr ""

#: src/components/SearchWarning.vue:12 src/components/SearchWarning.vue:13
#: src/components/SearchWarning.vue:18 src/components/SearchWarning.vue:20
#: src/components/SearchWarning.vue:24 src/components/SearchWarning.vue:27
msgid "Understood"
msgstr ""

#: src/components/result/PostResult.vue:29
#: src/components/result/PostResult.vue:30
#: src/components/result/PostResult.vue:36
#: src/components/result/PostResult.vue:42
#: src/components/result/PostResult.vue:45
msgid "Updated on"
msgstr ""

#: src/views/Home.vue:32 src/views/Home.vue:33 src/views/Home.vue:41
#: src/views/Home.vue:46 src/views/Home.vue:48
#, fuzzy
msgid "View all categories"
msgstr "Exibir todas as categorias"

#: src/components/local/CloseEvents.vue:21
#: src/components/local/CloseEvents.vue:22
#: src/components/local/CloseEvents.vue:26
#: src/components/local/CloseEvents.vue:27
#, fuzzy
msgid "View more events around %{position}"
msgstr "Assista o vídeo em %{host}"

#: src/components/result/EventResult.vue:68
#: src/components/result/EventResult.vue:77
#, fuzzy
msgid "View more events near %{locality}"
msgstr "Assista o vídeo em %{host}"

#: src/components/local/CloseGroups.vue:24
#: src/components/local/CloseGroups.vue:25
#: src/components/local/CloseGroups.vue:29
#: src/components/local/CloseGroups.vue:30
#, fuzzy
msgid "View more groups around %{position}"
msgstr "Assista o vídeo em %{host}"

#: src/components/result/GroupResult.vue:21
#: src/components/result/GroupResult.vue:57
msgid "View more groups near %{locality}"
msgstr ""

#: src/components/local/OnlineEvents.vue:25
#: src/components/local/OnlineEvents.vue:26
#: src/components/local/OnlineEvents.vue:29
#: src/components/local/OnlineEvents.vue:34
msgid "View more online events"
msgstr ""

#: src/components/Footer.vue:31 src/components/Footer.vue:32
#: src/components/Footer.vue:34 src/components/Footer.vue:36
#, fuzzy
msgid "Why should I have my own Mobilizon website?"
msgstr "Por que eu deveria ter minha própria instância no PeerTube?"

#: src/query/constants.ts:606
msgid "With the most participants"
msgstr ""

#: src/components/SetupInstanceSuggestion.vue:78
msgid "Your federated identity"
msgstr ""

#: src/components/search/SearchFields.vue:10
msgid "Your search"
msgstr ""

#: src/views/Search.vue:408 src/views/Search.vue:409 src/views/Search.vue:671
#: src/views/Search.vue:677 src/views/Search.vue:766 src/views/Search.vue:781
#: src/views/Search.vue:783 src/views/Search.vue:787
msgid "Your search did not match any events."
msgstr ""

#: src/views/Search.vue:420 src/views/Search.vue:421 src/views/Search.vue:683
#: src/views/Search.vue:689 src/views/Search.vue:778 src/views/Search.vue:793
#: src/views/Search.vue:799 src/views/Search.vue:803
msgid "Your search did not match any groups."
msgstr ""

#: src/views/Search.vue:432 src/views/Search.vue:433 src/views/Search.vue:695
#: src/views/Search.vue:701 src/views/Search.vue:790 src/views/Search.vue:805
#: src/views/Search.vue:815 src/views/Search.vue:819
msgid "Your search did not match any results."
msgstr ""

#: src/views/Search.vue:401 src/views/Search.vue:402 src/views/Search.vue:664
#: src/views/Search.vue:670 src/views/Search.vue:759 src/views/Search.vue:774
#: src/views/Search.vue:776 src/views/Search.vue:779
msgid "Your search for %{searchTerm} did not match any events."
msgstr ""

#: src/views/Search.vue:413 src/views/Search.vue:414 src/views/Search.vue:676
#: src/views/Search.vue:682 src/views/Search.vue:771 src/views/Search.vue:786
#: src/views/Search.vue:792 src/views/Search.vue:795
msgid "Your search for %{searchTerm} did not match any groups."
msgstr ""

#: src/views/Search.vue:425 src/views/Search.vue:426 src/views/Search.vue:688
#: src/views/Search.vue:694 src/views/Search.vue:783 src/views/Search.vue:798
#: src/views/Search.vue:808 src/views/Search.vue:811
msgid "Your search for %{searchTerm} did not match any results."
msgstr ""

#: src/query/constants.ts:36
msgid "ελληνικά"
msgstr ""

#: src/query/constants.ts:84
msgid "русский"
msgstr ""

#: src/query/constants.ts:16
msgid "日本語"
msgstr ""

#: src/query/constants.ts:64
msgid "简体中文（中国）"
msgstr ""

#: src/views/Search.vue:118 src/views/Search.vue:119 src/views/Search.vue:121
#: src/views/Search.vue:123 src/views/Search.vue:138 src/views/Search.vue:281
#: src/views/Search.vue:299 src/views/Search.vue:305
msgctxt "Categories"
msgid "All"
msgstr ""

#: src/views/Search.vue:177 src/views/Search.vue:178 src/views/Search.vue:180
#: src/views/Search.vue:182 src/views/Search.vue:197 src/views/Search.vue:391
#: src/views/Search.vue:408 src/views/Search.vue:414
msgctxt "Languages"
msgid "All"
msgstr ""

#: src/views/Search.vue:135 src/views/Search.vue:136 src/views/Search.vue:138
#: src/views/Search.vue:140 src/views/Search.vue:155 src/views/Search.vue:325
#: src/views/Search.vue:343
msgctxt "Statuses"
msgid "All"
msgstr ""

#, fuzzy
#~ msgid "No event found"
#~ msgstr "Nenhum resultado encontrado para"

#, fuzzy
#~ msgid "No group found"
#~ msgstr "Nenhum resultado encontrado para"

#, fuzzy
#~ msgid ""
#~ "A search engine of <a class=\"font-bold underline\" href=\"https://"
#~ "joinmobilizon.org\" target=\"_blank\">Mobilizon</a> events and groups"
#~ msgstr ""
#~ "Um mecanismo de busca de canais e vídeos de <a href=\"https://"
#~ "joinpeertube.org\" target=\"_blank\">PeerTube</a>"

#~ msgid "Reset"
#~ msgstr "Reiniciar"

#~ msgid "Last 30 days"
#~ msgstr "Últimos 30 dIas"

#~ msgid "Last 7 days"
#~ msgstr "Últimos 7 dIas"

#, fuzzy
#~ msgid "Last year"
#~ msgstr "Últimos 7 dIas"

#, fuzzy
#~ msgid "%{eventsCount} event found"
#~ msgid_plural "%{eventsCount} events found"
#~ msgstr[0] "%{resultsCount} resultado encontrado para"
#~ msgstr[1] "%{resultsCount} resultados encontrados para"

#, fuzzy
#~ msgid "%{groupsCount} group found"
#~ msgid_plural "%{groupsCount} groups found"
#~ msgstr[0] "%{resultsCount} resultado encontrado para"
#~ msgstr[1] "%{resultsCount} resultados encontrados para"

#, fuzzy
#~ msgid "%{resultsCount} result found"
#~ msgid_plural "%{resultsCount} results found"
#~ msgstr[0] "%{resultsCount} resultado encontrado para"
#~ msgstr[1] "%{resultsCount} resultados encontrados para"

#~ msgid "Add tag"
#~ msgstr "Adicionar tag"

#~ msgid "All of these tags"
#~ msgstr "Todas estas tags"

#~ msgid "Display all languages"
#~ msgstr "Exibir todos os idiomas"

#~ msgid "Filters"
#~ msgstr "Filtros"

#~ msgid "One of these tags"
#~ msgstr "Uma destas tags"

#, fuzzy
#~ msgid ""
#~ "%{instancesCount} Mobilizon websites indexed by %{indexName}!\" :"
#~ "class=\"{ 'hidden': !instancesCount }\" class=\"text-center max-w-lg mt-6 "
#~ "mb-12 mx-auto dark:text-zinc-300\" > Search for your favorite events and "
#~ "groups on <a class=\"underline\" href=\"%{indexedInstancesUrl}\" "
#~ "target=\"_blank\">one Mobilizon website</a> indexed by %{indexName}!"
#~ msgid_plural "Search for your favorite events and groups on <a class=\\"
#~ msgstr[0] ""
#~ "Busque seus canais e vídeos favoritos em <a "
#~ "href=\"%{indexedInstancesUrl}\" target=\"_blank\">%{instancesCount} "
#~ "instâncias de PeerTube</a> indexadas por %{indexName}!"
#~ msgstr[1] ""
#~ "Busque seus canais e vídeos favoritos em <a "
#~ "href=\"%{indexedInstancesUrl}\" target=\"_blank\">%{instancesCount} "
#~ "instâncias de PeerTube</a> indexadas por %{indexName}!"

#~ msgid "Activism"
#~ msgstr "Ativismo"

#~ msgid "Animals"
#~ msgstr "Animais"

#~ msgid "Art"
#~ msgstr "Arte"

#~ msgid "Attribution"
#~ msgstr "Atribuição"

#~ msgid "Attribution - No Derivatives"
#~ msgstr "Atribuição - Sem Derivações"

#~ msgid "Attribution - Non Commercial"
#~ msgstr "Atribuição - Não Comercial"

#~ msgid "Attribution - Non Commercial - No Derivatives"
#~ msgstr "Atribuição Não Comercial - Sem Derivações"

#~ msgid "Attribution - Non Commercial - Share Alike"
#~ msgstr "Atribuição - Não Comercial - Compartilha Igual"

#~ msgid "Attribution - Share Alike"
#~ msgstr "Atribuição - Compartilha Igual"

#~ msgid "Entertainment"
#~ msgstr "Entretenimento"

#~ msgid "Films"
#~ msgstr "Filmes"

#~ msgid "Food"
#~ msgstr "Comida"

#~ msgid "Gaming"
#~ msgstr "Gaming"

#~ msgid "How To"
#~ msgstr "Como fazer"

#, fuzzy
#~ msgid "Keyword, channel, video, playlist, etc."
#~ msgstr "Palavra chave, canal, video, etc."

#~ msgid "Kids"
#~ msgstr "Infantil"

#~ msgid "News & Politics"
#~ msgstr "Notícias e Política"

#~ msgid "People"
#~ msgstr "Pessoas"

#~ msgid "Public Domain Dedication"
#~ msgstr "Dedicado a Domínio Público"

#~ msgid "Travels"
#~ msgstr "Turismo"

#~ msgid "Vehicles"
#~ msgstr "Veículos"

#~ msgid "&gt;&gt; Check all guides on joinpeertube.org &lt;&lt;"
#~ msgstr "&gt;&gt; Visualize todos os guias em joinpeertube.org &lt;&lt;"

#~ msgid "A free software to take back control of your videos"
#~ msgstr "Um software livre para tomar de volta o controle dos seus videos"

#~ msgid "Create an account to take back control of your videos"
#~ msgstr "Crie uma conta para recuperar de volta o controle de seus vídeos"

#~ msgid ""
#~ "Developed by <a href=\"https://framasoft.org\" "
#~ "target=\"_blank\">Framasoft</a>"
#~ msgstr ""
#~ "Desenvolvido por <a href=\"https://framasoft.org\" "
#~ "target=\"_blank\">Framasoft</a>"

#~ msgid "Display homepage"
#~ msgstr "Exibir a página inicial"

#~ msgid "Duration"
#~ msgstr "Duração"

#~ msgid "Install PeerTube"
#~ msgstr "Instalar PeerTube"

#~ msgid "Last 365 days"
#~ msgstr "Últimos 365 dIas"

#~ msgid "Legal notices"
#~ msgstr "Avisos legais"

#~ msgid "Long (> 10 min)"
#~ msgstr "Grande (> 10 min)"

#~ msgid "Medium (4-10 min)"
#~ msgstr "Médio (4-10 min)"

#~ msgid "On"
#~ msgstr "Em"

#, fuzzy
#~ msgid "on %{instancesCount} indexed Mobilizon website"
#~ msgid_plural "on %{instancesCount} indexed Mobilizon websites"
#~ msgstr[0] "em %{instancesCount} página de PeerTube indexada"
#~ msgstr[1] "em %{instancesCount} páginas de PeerTube indexadas"

#~ msgid "Open an account on a PeerTube website"
#~ msgstr "Registrar uma conta em uma instância do PeerTube"

#~ msgid "Open your own videos website with PeerTube!"
#~ msgstr "Faça seu próprio site de vídeos com PeerTube!"

#~ msgid "Published date"
#~ msgstr "Data da publicação"

#~ msgid "Short (< 4 min)"
#~ msgstr "Curto (< 4 min)"

#~ msgid "Tags"
#~ msgstr "Tags"

#~ msgid "with %{activeFilters} active filter"
#~ msgid_plural "with %{activeFilters} active filters"
#~ msgstr[0] "com %{activeFilters} filtro ativado"
#~ msgstr[1] "com %{activeFilters} filtros ativados"

#~ msgid "Display all licenses"
#~ msgstr "Exibir todas as licenças"

#~ msgid "Display only"
#~ msgstr "Exibir apenas"

#~ msgid "Display sensitive content"
#~ msgstr "Exibir conteúdo sensível"

#~ msgid "Go on this channel page"
#~ msgstr "Ir para a página deste canal"

#~ msgid "Licence"
#~ msgstr "Licença"

#~ msgid "LIVE"
#~ msgstr "AO VIVO"

#~ msgid "Live videos"
#~ msgstr "Transmissões ao vivo"

#~ msgid "No"
#~ msgstr "Não"

#~ msgid "VOD videos"
#~ msgstr "Vídeos sob demanda (VOD)"

#~ msgid "Yes"
#~ msgstr "Sim"

#~ msgid "%{ channel.followersCount } follower"
#~ msgid_plural "%{channel.followersCount} followers"
#~ msgstr[0] "%{ channel.followersCount } seguidor"
#~ msgstr[1] "%{channel.followersCount} seguidores"
