import axios from 'axios'

const userInstanceRedirectionURILocalStorageKey = 'userInstanceRedirectionURI'
const userInstanceNameLocalStorageKey = 'userInstanceName'

export const ownInstanceRedirectURI = (): string | null => {
  return localStorage?.getItem(userInstanceRedirectionURILocalStorageKey)
}

export const ownInstanceName = (): string | null => {
  return localStorage?.getItem(userInstanceNameLocalStorageKey)
}

export const hostFromInstanceRedirectURI = (
  instanceRedirectURI: string
): string => {
  if (!instanceRedirectURI) return
  const url = new URL(instanceRedirectURI)
  return url.host
}

export const produceRedirectURL = (
  url: string,
  instanceRedirectionURI: string
): string => {
  return instanceRedirectionURI.replace('{uri}', encodeURIComponent(url))
}

const findRedirectionURI = async (
  remoteActorAddress: string
): Promise<string> => {
  return await webFingerFetch(
    hostFromFederatedUsername(remoteActorAddress),
    remoteActorAddress
  )
}

export const hostFromFederatedUsername = (
  federatedUsername: string
): string => {
  const [, host] = federatedUsername.split('@', 2)
  return host
}

export const saveRedirectionURI = async (
  remoteActorAddress: string
): Promise<string> => {
  const redirectionURI = await findRedirectionURI(remoteActorAddress)
  localStorage.setItem(
    userInstanceRedirectionURILocalStorageKey,
    redirectionURI
  )
  return redirectionURI
}

export const saveInstanceName = (instanceName: string) => {
  localStorage.setItem(userInstanceNameLocalStorageKey, instanceName)
}

const webFingerFetch = async (
  hostname: string,
  identity: string
): Promise<string> => {
  const res = await axios.get(
    `https://${hostname}/.well-known/webfinger?resource=acct:${identity}`
  )
  if (res.data && Array.isArray(res.data.links)) {
    const link: { template: string } = res.data.links.find(
      (someLink: any) =>
        someLink &&
        typeof someLink.template === 'string' &&
        someLink.rel === 'http://ostatus.org/schema/1.0/subscribe'
    )

    if (link && link.template.includes('{uri}')) {
      return link.template
    }
  }
  throw new Error('No interaction path found in webfinger data')
}

export const instanceNodeInfoMetadata = async (
  federatedUsername: string
): Promise<{ name: string | undefined; description: string | undefined }> => {
  const hostname = hostFromFederatedUsername(federatedUsername)
  const res = await axios.get(`https://${hostname}/.well-known/nodeinfo/2.1`)
  return {
    name: res?.data?.metadata?.nodeName,
    description: res?.data?.metadata?.nodeDescription,
  }
}

export const contentOnOwnInstanceURL = (
  url: string,
  instanceRedirectURI: string | undefined
): string => {
  if (instanceRedirectURI) {
    return produceRedirectURL(url, instanceRedirectURI)
  }
  return url
}

export const clearUserInstanceData = () => {
  localStorage.removeItem(userInstanceRedirectionURILocalStorageKey)
  localStorage.removeItem(userInstanceNameLocalStorageKey)
}
