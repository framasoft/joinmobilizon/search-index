import { App } from 'vue'

export default async function install(app: App, options: { locale: Locale }) {
  app.provide('dateFnsLocale', options.locale)
}

export async function loadLocale(localeCode: string): Promise<Locale> {
  return (
    await import(
      `../../node_modules/date-fns/esm/locale/${convertLocale(
        localeCode
      )}/index.js`
    )
  ).default
}

function convertLocale(locale: string) {
  switch (locale) {
    case 'fr_FR':
      return 'fr'
    default:
      return locale.replace('_', '-')
  }
}
