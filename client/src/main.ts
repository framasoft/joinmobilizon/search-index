import './scss/index.css'
import { createApp } from 'vue'
import VueMatomo from 'vue-matomo'
import AppWrapper from './AppWrapper.vue'
import { createRouter, createWebHistory } from 'vue-router'
import dateFns, { loadLocale } from './plugins/dateFns'
import 'flowbite'
import {
  generateGettext,
  currentLanguage,
  allLocales,
  loadNewTranslation,
} from './boot/i18n'
import clickOutside from '@/directives/clickOutside'
import Categories from './views/Categories.vue'
import Search from './views/Search.vue'
import HomeView from './views/HomeView.vue'
import { createHead } from '@vueuse/head'

async function main() {
  const app = createApp(AppWrapper)
  app.config.performance = true

  document.documentElement.setAttribute(
    'lang',
    currentLanguage.replace('_', '-')
  )

  const gettext = await generateGettext()
  app.use(gettext)
  app.directive('click-outside', clickOutside)

  // Routes
  const routes = [
    {
      name: 'SEARCH',
      path: '/:locale?/search',
      component: Search,
    },
    {
      name: 'CATEGORIES',
      path: '/:locale?/categories',
      component: Categories,
    },
    {
      name: 'HOME',
      path: '/:locale?',
      component: HomeView,
    },
    {
      path: '/*',
      // Don't want to create a 404 page
      component: HomeView,
    },
  ]
  const router = createRouter({
    history: createWebHistory(),
    routes,
  })
  router.beforeEach(async (to, from, next) => {
    const locale = to.params.locale as string
    if (locale && !allLocales.includes(locale)) {
      return next('en_US')
    }
    if (locale && allLocales.includes(locale) && gettext.current !== locale) {
      await loadNewTranslation(gettext, locale)
    }
    return next()
  })

  // Stats Matomo
  if (
    !(
      navigator.doNotTrack === 'yes' ||
      navigator.doNotTrack === '1' ||
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      window.doNotTrack === '1'
    )
  ) {
    app.use(VueMatomo, {
      // Configure your matomo server and site
      host: 'https://stats.framasoft.org/',
      siteId: 86,

      // Enables automatically registering pageviews on the router
      router,

      // Require consent before sending tracking information to matomo
      // Default: false
      requireConsent: false,

      // Whether to track the initial page view
      // Default: true
      trackInitialView: true,

      // Changes the default .js and .php endpoint's filename
      // Default: 'piwik'
      trackerFileName: 'p',

      enableLinkTracking: true,
    })

    const _paq = []

    // CNIL conformity
    _paq.push([
      function piwikCNIL() {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        const { getVisitorInfo } = this

        function getOriginalVisitorCookieTimeout() {
          const now = new Date()
          const nowTs = Math.round(now.getTime() / 1000)
          const visitorInfo = getVisitorInfo()
          const createTs = parseInt(visitorInfo[2], 10)
          const cookieTimeout = 33696000 // 13 months in seconds
          return createTs + cookieTimeout - nowTs
        }

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout())
      },
    ])
  }

  const head = createHead()
  app.use(head)

  app.use(router)

  const locale = await loadLocale(currentLanguage)
  app.use(dateFns, { locale })
  app.mount('#app')
}

main()
