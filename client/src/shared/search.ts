import axios from 'axios'
import { ResultList } from '../../../server/types/search-query/common-search.model'
import { EventsSearchQuery } from '../../../server/types/search-query/event-search.model'
import { GroupsSearchQuery } from '../../../server/types/search-query/group-search.model'
import { EnhancedGroup } from '../../../server/types/group.model'
import { EnhancedEvent } from '../../../server/types/event.model'
import { buildApiUrl } from './utils'
import {
  AutoCompleteLocation,
  Location,
} from '../../../server/types/address.model'
import { PictureInformation } from '../../../shared/picture'

const baseEventsPath = '/api/v1/search/events'
const baseEventMarkersPath = '/api/v1/search/event-markers'
const baseGroupMarkersPath = '/api/v1/search/group-markers'
const baseGroupsPath = '/api/v1/search/groups'
const baseLocationsPath = '/api/v1/search/locations'
const baseGeoLocationsPath = '/api/v1/location'

function searchEvents(params: EventsSearchQuery) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })
  return axios
    .get<ResultList<EnhancedEvent>>(buildApiUrl(baseEventsPath), options)
    .then((res) => res.data)
}

function searchGroups(params: GroupsSearchQuery) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })

  return axios
    .get<ResultList<EnhancedGroup>>(buildApiUrl(baseGroupsPath), options)
    .then((res) => res.data)
}

async function searchLocations(search: string) {
  const res = await axios.get<ResultList<AutoCompleteLocation>>(
    buildApiUrl(baseLocationsPath),
    { params: { search } }
  )
  return res.data
}

async function searchEventMarkers(params: any) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })

  const res = await axios.get<ResultList<any>>(
    buildApiUrl(baseEventMarkersPath),
    options
  )
  return res.data
}

async function searchGroupMarkers(params: any) {
  const options = {
    params,
  }

  if (params.search) Object.assign(options.params, { search: params.search })

  const res = await axios.get<ResultList<any>>(
    buildApiUrl(baseGroupMarkersPath),
    options
  )
  return res.data
}

async function getGeoLocationInformation(
  latitude: number,
  longitude: number
): Promise<Location & { picture_info?: PictureInformation }> {
  const res = await axios.get<Location & { picture_info?: PictureInformation }>(
    buildApiUrl(baseGeoLocationsPath),
    { params: { latitude, longitude } }
  )
  return res.data
}

export {
  searchEvents,
  searchGroups,
  searchLocations,
  searchEventMarkers,
  searchGroupMarkers,
  getGeoLocationInformation,
}
