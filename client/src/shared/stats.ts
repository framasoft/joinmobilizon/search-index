import axios from 'axios'
import { buildApiUrl } from './utils'
import { CategoryStatsModel } from '../../../shared/stats.model'
import { boostLanguagesQuery } from '@/query/misc'

const basePath = '/api/v1/stats/categories'

async function getCategoryStats($gettext) {
  const res = await axios.get<CategoryStatsModel[]>(
    buildApiUrl(
      basePath +
        `?${boostLanguagesQuery($gettext)
          .map((lang) => `languageOneOf[]=${lang}`)
          .join('&')}`
    )
  )
  return res.data
}

export { getCategoryStats }
