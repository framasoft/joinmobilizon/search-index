import {
  addDays,
  addMonths,
  addWeeks,
  eachWeekendOfInterval,
  endOfDay,
  endOfMonth,
  endOfToday,
  endOfWeek,
  format,
  startOfDay,
  startOfMonth,
  startOfWeek,
} from 'date-fns'
import { random } from 'lodash'

function buildApiUrl(path: string) {
  const normalizedPath = path.startsWith('/') ? path : '/' + path

  const base = import.meta.env.VITE_API_URL || ''
  return base + normalizedPath
}

function pageToAPIParams(page: number, itemsPerPage: number) {
  const start = (page - 1) * itemsPerPage
  const count = itemsPerPage

  return { start, count }
}

function startDateRangeToAPIParams(durationRange: string, locale: Locale) {
  if (!durationRange) {
    return { startDateMin: undefined, startDateMax: undefined }
  }

  switch (durationRange) {
    case 'past':
      return { startDateMin: undefined, startDateMax: new Date().toISOString() }

    case 'future':
      return { startDateMin: new Date().toISOString(), startDateMax: undefined }

    case 'today':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfToday().toISOString(),
      }

    case 'tomorrow':
      return {
        startDateMin: startOfDay(addDays(new Date(), 1)).toISOString(),
        startDateMax: endOfDay(addDays(new Date(), 1)).toISOString(),
      }

    case 'week_end':
      return weekend(locale)

    case 'week':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfWeek(addDays(new Date(), 1), {
          locale,
        }).toISOString(),
      }

    case 'next_week':
      return {
        startDateMin: startOfWeek(addWeeks(new Date(), 1), {
          locale,
        }).toISOString(),
        startDateMax: endOfWeek(addWeeks(new Date(), 1), {
          locale,
        }).toISOString(),
      }

    case 'month':
      return {
        startDateMin: new Date().toISOString(),
        startDateMax: endOfMonth(new Date()).toISOString(),
      }

    case 'next_month':
      return {
        startDateMin: startOfMonth(addMonths(new Date(), 1)).toISOString(),
        startDateMax: endOfMonth(addMonths(new Date(), 1)).toISOString(),
      }

    case 'any_date':
      return { startDateMin: undefined, startDateMax: undefined }

    default:
      console.error('Unknown duration range %s', durationRange)
      return { startDateMin: undefined, startDateMax: undefined }
  }
}

function weekend(locale: Locale) {
  const now = new Date()
  const endOfWeekDate = endOfWeek(now, { locale })
  const startOfWeekDate = startOfWeek(now, { locale })
  const [start, end] = eachWeekendOfInterval({
    start: startOfWeekDate,
    end: endOfWeekDate,
  })
  return {
    startDateMin: startOfDay(start).toISOString(),
    startDateMax: endOfDay(end).toISOString(),
  }
}

function publishedDateRangeToAPIParams(publishedDateRange: string) {
  if (!publishedDateRange) {
    return { startDate: undefined, endDate: undefined }
  }

  // today
  const date = new Date()
  date.setHours(0, 0, 0, 0)

  switch (publishedDateRange) {
    case 'published_today':
      break

    case 'last_week':
      date.setDate(date.getDate() - 7)
      break

    case 'last_month':
      date.setDate(date.getDate() - 30)
      break

    case 'last_year':
      date.setDate(date.getDate() - 365)
      break

    default:
      console.error('Unknown published date range %s', publishedDateRange)
      return { startDate: undefined, endDate: undefined }
  }

  return { startDate: date.toISOString(), endDate: undefined }
}

function extractArrayParamFromQuery(value: string | string[]): string[] {
  if (!value) return []

  if (Array.isArray(value)) return value
  return [value]
}

function formatDateTime(dateTime: Date, locale): string {
  return format(dateTime, 'PPp', { locale })
}

const randomGradient = (): string => {
  const direction = [
    'bg-gradient-to-t',
    'bg-gradient-to-tr',
    'bg-gradient-to-r',
    'bg-gradient-to-br',
    'bg-gradient-to-b',
    'bg-gradient-to-bl',
    'bg-gradient-to-l',
    'bg-gradient-to-tl',
  ]
  const gradients = [
    'from-pink-500 via-red-500 to-yellow-500',
    'from-green-400 via-blue-500 to-purple-600',
    'from-pink-400 via-purple-300 to-indigo-400',
    'from-yellow-300 via-yellow-500 to-yellow-700',
    'from-yellow-300 via-green-400 to-green-500',
    'from-red-400 via-red-600 to-yellow-400',
    'from-green-400 via-green-500 to-blue-700',
    'from-yellow-400 via-yellow-500 to-yellow-700',
    'from-green-300 via-green-400 to-purple-700',
  ]
  return `${direction[random(0, direction.length - 1)]} ${
    gradients[random(0, gradients.length - 1)]
  }`
}

function closest(
  needle: number,
  haystack: number[] = [5, 10, 25, 50, 100, 150]
) {
  return haystack.reduce((a, b) => {
    const aDiff = Math.abs(a - needle)
    const bDiff = Math.abs(b - needle)

    if (aDiff == bDiff) {
      return a > b ? a : b
    } else {
      return bDiff < aDiff ? b : a
    }
  })
}

export {
  buildApiUrl,
  publishedDateRangeToAPIParams,
  pageToAPIParams,
  startDateRangeToAPIParams,
  extractArrayParamFromQuery,
  formatDateTime,
  randomGradient,
  closest,
}
