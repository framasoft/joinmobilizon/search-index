export enum ContentType {
  ALL = 'ALL',
  EVENTS = 'EVENTS',
  GROUPS = 'GROUPS',
}
