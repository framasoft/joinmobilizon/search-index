import { pageToAPIParams, startDateRangeToAPIParams } from '@/shared/utils'
import { EventsSearchQuery } from '../../../server/types/search-query/event-search.model'
import { GroupsSearchQuery } from '../../../server/types/search-query/group-search.model'
import {
  boostLanguagesQuery,
  GeographicBounds,
  SearchFormEventData,
  SearchFormGroupData,
} from '@/query/misc'
import { ContentType } from '@/models'
import { sortOptions, SortValues } from './constants'

export function buildCommonEventSearchQuery(
  formData: SearchFormEventData & { locale: Locale },
  $gettext: any
) {
  const { startDateMin, startDateMax } = startDateRangeToAPIParams(
    formData.eventStartDateRange,
    formData.locale
  )

  const latitude = formData.locationData?.lat
  const longitude = formData.locationData?.lon

  return {
    search: formData.search !== '' ? formData.search : undefined,

    startDateMin,
    startDateMax,

    boostLanguages: boostLanguagesQuery($gettext),

    categoryOneOf: formData.categoryOneOf,
    languageOneOf: formData.languageOneOf,

    // tagsOneOf: this.formTagsOneOf.map((t) => t.text),
    // tagsAllOf: this.formTagsAllOf.map((t) => t.text),

    // host,

    latlon:
      latitude && longitude && !formData.isOnline
        ? `${latitude}:${longitude}`
        : undefined,
    distance: !formData.isOnline ? formData.distance : undefined,

    sort: sortOptions($gettext, ContentType.EVENTS)
      .map((sortOption) => sortOption.key)
      .includes(formData.sort as SortValues)
      ? formData.sort
      : undefined,
    isOnline: formData.isOnline ? true : undefined,
    statusOneOf: formData.statusOneOf,
  }
}

export function buildEventSearchQuery(formData, $gettext) {
  const { start, count } = pageToAPIParams(
    formData.page,
    formData.resultsPerEventsPage
  )

  return {
    ...buildCommonEventSearchQuery(formData, $gettext),
    start,
    count,
  } as EventsSearchQuery
}

export function buildGroupSearchQuery(formData: SearchFormGroupData, $gettext) {
  const { start, count } = pageToAPIParams(
    formData.page,
    formData.resultsPerGroupPage
  )

  const latitude = formData.locationData?.lat
  const longitude = formData.locationData?.lon

  console.debug('buildGroupSearchQuery', formData)

  return {
    search: formData.search !== '' ? formData.search : undefined,
    // host,

    boostLanguages: boostLanguagesQuery($gettext),
    languageOneOf: formData.languageOneOf,

    latlon: latitude && longitude ? `${latitude}:${longitude}` : undefined,
    distance: formData.distance,
    sort: sortOptions($gettext, ContentType.GROUPS)
      .map((sortOption) => sortOption.key)
      .includes(formData.sort as SortValues)
      ? formData.sort
      : undefined,
    start,
    count,
  } as GroupsSearchQuery
}

export function buildEventMarkerSearchQuery(
  params: SearchFormEventData & GeographicBounds & { locale: Locale },
  $gettext
) {
  return {
    ...buildCommonEventSearchQuery(params, $gettext),
    zoom: params.zoom,
    bbox: params.bbox,
  }
}
