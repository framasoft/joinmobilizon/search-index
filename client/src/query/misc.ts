import { eventLanguages } from '@/query/constants'
import { EventStatus } from '../../../server/types/event.model'
import { ContentType } from '@/models'
import { LocationType } from '../../../shared/user-location.model'

export function boostLanguagesQuery($gettext): string[] {
  const languages = new Set<string>()

  for (const completeLanguage of navigator.languages) {
    const language = completeLanguage.split('-')[0]

    if (eventLanguages($gettext).find((vl) => vl.id === language)) {
      languages.add(language)
    }
  }

  return Array.from(languages)
}

export interface GeographicBounds {
  geohash?: string
  bbox?: string
  zoom?: number
}

export interface SearchFormData {
  search?: string
  sort?: string
  page: number
  contentType?: ContentType
  distance?: string
  locationData?: LocationType
  languageOneOf?: string[]
}

export interface SearchFormEventData extends SearchFormData {
  eventStartDateRange: string
  resultsPerEventPage?: number
  categoryOneOf: string[]
  isOnline: boolean
  statusOneOf: EventStatus[]
}

export interface SearchFormGroupData extends SearchFormData {
  resultsPerGroupPage?: number
}

export interface SearchFormAllData
  extends SearchFormEventData,
    SearchFormGroupData {}
