import { ContentType } from '@/models'
import { EventStatus } from '../../../server/types/event.model'

export const eventLanguages = ($gettext) => {
  return [
    {
      id: 'en',
      label: $gettext('English'),
    },
    {
      id: 'fr',
      label: $gettext('Français'),
    },
    {
      id: 'ja',
      label: $gettext('日本語'),
    },
    {
      id: 'eu',
      label: $gettext('Euskara'),
    },
    {
      id: 'ca',
      label: $gettext('Català'),
    },
    {
      id: 'cs',
      label: $gettext('Čeština'),
    },
    {
      id: 'eo',
      label: $gettext('Esperanto'),
    },
    {
      id: 'el',
      label: $gettext('ελληνικά'),
    },
    {
      id: 'de',
      label: $gettext('Deutsch'),
    },
    {
      id: 'it',
      label: $gettext('Italiano'),
    },
    {
      id: 'nl',
      label: $gettext('Nederlands'),
    },
    {
      id: 'es',
      label: $gettext('Español'),
    },
    {
      id: 'oc',
      label: $gettext('Occitan'),
    },
    {
      id: 'gd',
      label: $gettext('Gàidhlig'),
    },
    {
      id: 'zh',
      label: $gettext('简体中文（中国）'),
    },
    {
      id: 'pt',
      label: $gettext('Português (Portugal)'),
    },
    {
      id: 'sv',
      label: $gettext('svenska'),
    },
    {
      id: 'pl',
      label: $gettext('Polski'),
    },
    {
      id: 'fi',
      label: $gettext('suomi'),
    },
    {
      id: 'ru',
      label: $gettext('русский'),
    },
  ]
}

export const eventLanguagesLabel = (
  category: string,
  $gettext
): string | undefined => {
  return eventLanguages($gettext).find(({ id }) => id === category)?.label
}

export const contentTypeMapping = ($gettext) => {
  return [
    {
      contentType: 'ALL',
      label: $gettext('Everything'),
    },
    {
      contentType: 'EVENTS',
      label: $gettext('Events'),
    },
    {
      contentType: 'GROUPS',
      label: $gettext('Groups'),
    },
  ]
}

export const eventCategories = ($gettext) => {
  return [
    {
      id: 'ARTS',
      label: $gettext('Arts'),
      icon: 'palette',
    },
    {
      id: 'BOOK_CLUBS',
      label: $gettext('Book Clubs'),
      icon: 'favourite-book',
    },
    {
      id: 'BUSINESS',
      label: $gettext('Business'),
    },
    {
      id: 'CAUSES',
      label: $gettext('Causes'),
    },
    {
      id: 'COMEDY',
      label: $gettext('Comedy'),
    },
    {
      id: 'CRAFTS',
      label: $gettext('Crafts'),
    },
    {
      id: 'FOOD_DRINK',
      label: $gettext('Food & Drink'),
    },
    {
      id: 'HEALTH',
      label: $gettext('Health'),
    },
    {
      id: 'MUSIC',
      label: $gettext('Music'),
    },
    {
      id: 'AUTO_BOAT_AIR',
      label: $gettext('Auto, Boat & Air'),
    },
    {
      id: 'COMMUNITY',
      label: $gettext('Community'),
    },
    {
      id: 'FAMILY_EDUCATION',
      label: $gettext('Family & Education'),
    },
    {
      id: 'FASHION_BEAUTY',
      label: $gettext('Fashion & Beauty'),
    },
    {
      id: 'FILM_MEDIA',
      label: $gettext('Film & Media'),
    },
    {
      id: 'GAMES',
      label: $gettext('Games'),
    },
    {
      id: 'LANGUAGE_CULTURE',
      label: $gettext('Language and Culture'),
    },
    {
      id: 'LEARNING',
      label: $gettext('Learning'),
    },
    {
      id: 'LGBTQ',
      label: $gettext('LGBTQ'),
    },
    {
      id: 'MOVEMENTS_POLITICS',
      label: $gettext('Movements and politics'),
    },
    {
      id: 'NETWORKING',
      label: $gettext('Networking'),
    },
    {
      id: 'PARTY',
      label: $gettext('Party'),
    },
    {
      id: 'PERFORMING_VISUAL_ARTS',
      label: $gettext('Performing & visual arts'),
    },
    {
      id: 'PETS',
      label: $gettext('Pets'),
    },
    {
      id: 'PHOTOGRAPHY',
      label: $gettext('Photography'),
    },
    {
      id: 'OUTDOORS_ADVENTURE',
      label: $gettext('Outdoors and adventure'),
    },
    {
      id: 'SPIRITUALITY_RELIGION_BELIEFS',
      label: $gettext('Spirituality, religion and beliefs'),
    },
    {
      id: 'SCIENCE_TECH',
      label: $gettext('Science and tech'),
    },
    {
      id: 'SPORTS',
      label: $gettext('Sports'),
    },
    {
      id: 'THEATRE',
      label: $gettext('Theatre'),
    },
    {
      id: 'MEETING',
      label: $gettext('Other'),
    },
  ].sort(({ label: label1 }, { label: label2 }) => label1.localeCompare(label2))
}

export const eventCategoryLabel = (
  category: string,
  $gettext
): string | undefined => {
  return eventCategories($gettext).find(({ id }) => id === category)?.label
}

export type CategoryPictureLicencingElement = { name: string; url: string }
export type CategoryPictureLicencing = {
  author: CategoryPictureLicencingElement
  source: CategoryPictureLicencingElement
  license?: CategoryPictureLicencingElement
}

export const categoriesPicturesLicences: Record<
  string,
  CategoryPictureLicencing
> = {
  THEATRE: {
    author: {
      name: 'David Joyce',
      url: 'https://www.flickr.com/photos/deapeajay/',
    },
    source: {
      name: 'Flickr',
      url: 'https://www.flickr.com/photos/30815420@N00/2213310171',
    },
    license: {
      name: 'CC BY-SA',
      url: 'https://creativecommons.org/licenses/by-sa/2.0/',
    },
  },
  SPORTS: {
    author: {
      name: 'Md Mahdi',
      url: 'https://unsplash.com/@mahdi17',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/lQpFRPrepQ8',
    },
  },
  MUSIC: {
    author: {
      name: 'Michael Starkie',
      url: 'https://unsplash.com/@starkie_pics',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/YjtevpXFHQY',
    },
  },
  ARTS: {
    author: {
      name: 'RhondaK Native Florida Folk Artist',
      url: 'https://unsplash.com/@rhondak',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/_Yc7OtfFn-0',
    },
  },
  SPIRITUALITY_RELIGION_BELIEFS: {
    author: {
      name: 'The Dancing Rain',
      url: 'https://unsplash.com/@thedancingrain',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/_KPuV9qSSlU',
    },
  },
  MOVEMENTS_POLITICS: {
    author: {
      name: 'Kyle Fiori',
      url: 'https://unsplash.com/@navy99',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/moytQ7vzhAM',
    },
  },
  PARTY: {
    author: {
      name: 'Amy Shamblen',
      url: 'https://unsplash.com/@amyshamblen',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/pJ_DCj9KswI',
    },
  },
  BUSINESS: {
    author: {
      name: 'Simone Hutsch',
      url: 'https://unsplash.com/@heysupersimi',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/6-c8GV2MBmg',
    },
  },
  FILM_MEDIA: {
    author: {
      name: 'Dan Senior',
      url: 'https://unsplash.com/@dansenior',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/ENtn4fH8C3g',
    },
  },
  PHOTOGRAPHY: {
    author: {
      name: 'Nathyn Masters',
      url: 'https://unsplash.com/@nathynmasters',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/k3oSs0hWOPo',
    },
  },
  HEALTH: {
    author: {
      name: 'Derek Finch',
      url: 'https://unsplash.com/@drugwatcher',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/Gi8Q8IfpxdY',
    },
  },
  GAMES: {
    author: {
      name: 'Randy Fath',
      url: 'https://unsplash.com/@randyfath',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/_EoxKxrDL20',
    },
  },
  OUTDOORS_ADVENTURE: {
    author: {
      name: 'Davide Sacchet',
      url: 'https://unsplash.com/@davide_sak_',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/RYN-kov1lTY',
    },
  },
  FOOD_DRINK: {
    author: {
      name: 'sina piryae',
      url: 'https://unsplash.com/@sinapiryae',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/bBzjWthTqb8',
    },
  },
  CRAFTS: {
    author: {
      name: 'rocknwool',
      url: 'https://unsplash.com/@rocknwool',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/Jcb5O26G08A',
    },
  },
  LGBTQ: {
    author: {
      name: 'analuisa gamboa',
      url: 'https://unsplash.com/@anigmb',
    },
    source: {
      name: 'Unsplash',
      url: 'https://unsplash.com/photos/HsraPtCtRCM',
    },
  },
}

export const categoriesWithPictures = [
  'SPORTS',
  'THEATRE',
  'MUSIC',
  'ARTS',
  'MOVEMENTS_POLITICS',
  'SPIRITUALITY_RELIGION_BELIEFS',
  'PARTY',
  'BUSINESS',
  'FILM_MEDIA',
  'PHOTOGRAPHY',
  'HEALTH',
  'GAMES',
  'OUTDOORS_ADVENTURE',
  'FOOD_DRINK',
  'CRAFTS',
  'LGBTQ',
]

export const eventStartDateRanges = ($gettext) => {
  return [
    {
      id: 'any_date',
      label: $gettext('Any'),
    },
    {
      id: 'past',
      label: $gettext('In the past'),
    },
    {
      id: 'future',
      label: $gettext('In the future'),
    },
    {
      id: 'today',
      label: $gettext('Today'),
    },
    {
      id: 'tomorrow',
      label: $gettext('Tomorrow'),
    },
    {
      id: 'week_end',
      label: $gettext('This weekend'),
    },
    {
      id: 'week',
      label: $gettext('This week'),
    },
    {
      id: 'next_week',
      label: $gettext('Next week'),
    },
    {
      id: 'month',
      label: $gettext('This month'),
    },
    {
      id: 'next_month',
      label: $gettext('Next month'),
    },
  ]
}

export const eventStartDateRangesLabel = (
  eventStartDateRange: string,
  $gettext
): string => {
  return eventStartDateRanges($gettext).find(
    ({ id }) => id === eventStartDateRange
  )?.label
}

export const eventDistance = ($gettext, $ngettext) => {
  return [
    {
      id: 'anywhere',
      label: $gettext('Any distance'),
    },
    {
      id: '5_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 5, {
        number: '5',
      }),
    },
    {
      id: '10_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 10, {
        number: '10',
      }),
    },
    {
      id: '25_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 25, {
        number: '25',
      }),
    },
    {
      id: '50_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 50, {
        number: '50',
      }),
    },
    {
      id: '100_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 100, {
        number: '100',
      }),
    },
    {
      id: '150_km',
      label: $ngettext('one kilometer', '%{number} kilometers', 150, {
        number: '150',
      }),
    },
  ]
}

export const eventDistanceLabel = (
  distance: string,
  $gettext,
  $ngettext
): string => {
  return eventDistance($gettext, $ngettext).find(({ id }) => id === distance)
    ?.label
}

export const eventStatuses = ($gettext) => {
  return [
    {
      id: EventStatus.CONFIRMED,
      label: $gettext('Confirmed'),
    },
    {
      id: EventStatus.TENTATIVE,
      label: $gettext('Tentative'),
    },
    {
      id: EventStatus.CANCELLED,
      label: $gettext('Cancelled'),
    },
  ]
}

export const eventStatusesLabel = (status: string, $gettext): string => {
  return eventStatuses($gettext).find(({ id }) => id === status)?.label
}

export enum SortValues {
  MATCH_DESC = '-match',
  START_TIME_ASC = 'startTime',
  START_TIME_DESC = '-startTime',
  CREATED_AT_DESC = '-createdAt',
  CREATED_AT_ASC = 'createdAt',
  PARTICIPANT_COUNT_DESC = '-participantCount',
  MEMBER_COUNT_DESC = '-memberCount',
}

export const sortOptions = ($gettext, contentType: ContentType) => {
  const options = [
    {
      key: SortValues.MATCH_DESC,
      label: $gettext('Best match'),
    },
  ]

  if (contentType == ContentType.EVENTS) {
    options.push(
      {
        key: SortValues.START_TIME_ASC,
        label: $gettext('Event date'),
      },
      {
        key: SortValues.CREATED_AT_DESC,
        label: $gettext('Most recently published'),
      },
      {
        key: SortValues.CREATED_AT_ASC,
        label: $gettext('Least recently published'),
      },
      {
        key: SortValues.PARTICIPANT_COUNT_DESC,
        label: $gettext('With the most participants'),
      }
    )
  }

  if (contentType == ContentType.GROUPS) {
    options.push({
      key: SortValues.MEMBER_COUNT_DESC,
      label: $gettext('Number of members'),
    })
  }

  return options
}
