const plugin = require('tailwindcss/plugin')

const hoverPlugin = plugin(function ({ addVariant, e, postcss }) {
  addVariant('hover', ({ container, separator }) => {
    const hoverRule = postcss.atRule({
      name: 'media',
      params: '(hover: hover)',
    })
    hoverRule.append(container.nodes)
    container.append(hoverRule)
    hoverRule.walkRules((rule) => {
      rule.selector = `.${e(
        `hover${separator}${rule.selector.slice(1)}`
      )}:hover`
    })
  })
})

module.exports = {
  content: ['./src/**/*.{html,ts,vue}', './node_modules/flowbite/**/*.js'],
  darkMode: 'media',
  theme: {
    extend: {
      screens: {
        '3xl': '1600px',
      },
      colors: {
        'frama-violet': '#725794',
        'frama-orange': '#cc4e13',
        'mbz-yellow': '#ffd599',
        'mbz-purple': '#424056',
        'mbz-bluegreen': '#1e7d97',
        primary: '#272633',
        secondary: '#ED8D07',
      },
      transitionProperty: {
        height: 'height',
        spacing: 'margin, padding',
      },
    },
  },
  plugins: [
    require('flowbite/plugin'),
    require('@tailwindcss/forms'),
    require('tailwind-children'),
    hoverPlugin,
  ],
}
