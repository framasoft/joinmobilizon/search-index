module.exports = {
  output: {
    path: './src/locale', // output path of all created files
    potPath: './messages.pot', // relative to output.path, so by default "./src/language/messages.pot"
    jsonPath: './', // relative to output.path, so by default "./src/language/translations.json"
    splitJson: true,
    flat: false, // don't create subdirectories for locales
    linguas: true, // create a LINGUAS file
    locales: [
      'bn',
      'de',
      'el',
      'en_US',
      'es',
      'fr_FR',
      'fy',
      'gd',
      'gl',
      'id',
      'it',
      'ja',
      'nl',
      'oc',
      'pl',
      'pt_BR',
      'ru',
      'sq',
      'sv',
      'th',
    ],
  },
}
