import { GeoIPResponse } from '../server/types/geo-ip'

export interface ServerConfig {
  searchInstanceName: string
  searchInstanceNameImage: string

  searchInstanceSearchImage: string

  indexedHostsCount: number

  indexedInstancesUrl: string

  legalNoticesUrl: string

  ipLocation: GeoIPResponse | null
}
