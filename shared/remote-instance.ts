export interface RemoteInstanceLanguage {
  displayName: string
  code: string
}

export interface RemoteInstance {
  id: number
  host: string
  name: string
  shortDescription: string
  version: string
  signupAllowed: boolean
  totalUsers: number
  totalEvents: number
  totalLocalEvents: number
  totalGroups: number
  totalLocalGroups: number
  totalInstanceFollowers: number
  totalInstanceFollowing: number
  supportsIPv6: boolean
  country: string
  languages: RemoteInstanceLanguage[]
  health: number
  createdAt: string
}

export interface RemoteInstanceBasic {
  name: string
  description: string
  host: string
}

export interface UserInstanceData {
  instanceRedirectURI: string
  instanceName: string
}
